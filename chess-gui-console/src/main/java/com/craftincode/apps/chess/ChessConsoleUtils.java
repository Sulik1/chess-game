package com.craftincode.apps.chess;

import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.piece.Piece;
import com.craftincode.apps.chess.piece.PieceSet;
import com.diogonunes.jcdp.color.ColoredPrinter;

import java.util.stream.IntStream;

public class ChessConsoleUtils {
    private static ColoredPrinter cp = new ColoredPrinter.Builder(1, false).build();

    public static void printBoard(ChessGameBoard board, boolean reverse) {
        int reverseRow, rowLabel,backgroundSwitch;
        Piece piece;
        String pieceText, backgroundText;
        for (int row = 0; row < 8; row++) {
            rowLabel = reverse ? row + 1 : 8 - row;
            System.out.println("");
//            System.out.println("  ----------------------");
            System.out.print(rowLabel + ":");
            for (int column = 0; column < 8; column++) {
                reverseRow = reverse ? 8 - row - 1 : row;
                piece = board.getPiece(column, reverseRow);
                backgroundSwitch = (row%2 + column%2)%2;
                backgroundText = backgroundSwitch == 0 ? "\u2591" : "\u2592";
                System.out.print("|");
                pieceText = pieceAsText(piece);
                System.out.print(pieceText.equals("") ?
                        backgroundText :
                        pieceAsText(piece));
            }
            System.out.print("|");
        }
        System.out.println();
        System.out.println("  ----------------------");
        System.out.print(" ");
        IntStream.range(65, 73).forEach(i -> System.out.print(" " + (char) i + " "));
    }

    private static String pieceAsText(Piece p) {
        String result = "";
        if (p == null) return result;
        switch (p.getPieceType()) {
            case KING:
                return p.getPieceSet().equals(PieceSet.WHITE) ? "\u2654" : "\u265A";
            case QUEEN:
                return p.getPieceSet().equals(PieceSet.WHITE) ? "\u2655" : "\u265B";
            case ROOK:
                return p.getPieceSet().equals(PieceSet.WHITE) ? "\u2656" : "\u265C";
            case BISHOP:
                return p.getPieceSet().equals(PieceSet.WHITE) ? "\u2657" : "\u265D";
            case KNIGHT:
                return p.getPieceSet().equals(PieceSet.WHITE) ? "\u2658" : "\u265E";
            case PAWN:
                return p.getPieceSet().equals(PieceSet.WHITE) ? "\u2659" : "\u265F";
        }
        return result;
    }
}