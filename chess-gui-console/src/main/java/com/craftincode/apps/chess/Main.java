package com.craftincode.apps.chess;

import com.craftincode.apps.chess.piece.PieceSet;

public class Main {
    public static void main(String[] args) {
        ConsoleApp app = new ConsoleApp(new Player("Kuba", PieceSet.WHITE), new Player("Filip",PieceSet.BLACK));
        app.play();
    }
}
