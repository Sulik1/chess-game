package com.craftincode.apps.chess;

import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.logic.CheckOrCheckMate;
import com.craftincode.apps.chess.logic.Move;
import com.craftincode.apps.chess.logic.MoveValidationFabric;
import com.craftincode.apps.chess.logic.validation.KingMoveValidator;
import com.craftincode.apps.chess.logic.validation.MoveValidator;
import com.craftincode.apps.chess.logic.validation.PawnMoveValidator;
import com.craftincode.apps.chess.piece.PieceSet;

import java.util.Scanner;

public class ConsoleApp {
    ChessGameBoard board;
    Player player1 ;
    Player player2;
    MoveValidationFabric mvF = new MoveValidationFabric();
    MoveValidator mv ;
    CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();



    public ConsoleApp(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        board = new ChessGameBoard();
    }

    public void play() {
        Player currentPlayer = player1;
        Scanner scanner = new Scanner(System.in);
        boolean reverse = false;
        boolean play = true;
        boolean isWhiteMove;
        while (play) {
            if (currentPlayer.getPieceSet()== PieceSet.WHITE){
                isWhiteMove=true;
            }else {
                isWhiteMove=false;
            }
            if (checkOrCheckMate.isCheck(board)){
                if (checkOrCheckMate.isCheckMate(board,isWhiteMove)){
                    if (currentPlayer==player1){
                        System.out.println("Wygrał "+player2.getName());
                    }else {
                        System.out.println("Wygrał "+player1.getName());
                    }
                    play=false;
                }
                else {
                    System.out.println("SZACH");
                    String coordinates = scanner.nextLine();
                    ChessConsoleUtils.printBoard(board, reverse);
                    System.out.println();
                    System.out.println(currentPlayer.getName() + " twoj ruch");
                    String[] coordinate = coordinates.split(" ");
                    String from = coordinate[0];
                    String to = coordinate[1];
                    Move move = new Move(from, to);
                    System.out.println(move);
                    mv = mvF.raturnValidatorForPiece(board,move);
                    if (mv != new KingMoveValidator()||!to.equals(checkOrCheckMate.whereIsKing(board,isWhiteMove))){
                        System.out.println("Zly ruch jeszcze raz");
                    }
                }
            }else {
                ChessConsoleUtils.printBoard(board, reverse);
                System.out.println();
                System.out.println(currentPlayer.getName() + " twoj ruch");
                System.out.println("podaj ruch w formacie np. H5 C3");
                String coordinates = scanner.nextLine();  //"H5 C3" ->"h5"
                String[] coordinate = coordinates.split(" ");
                String from = coordinate[0];
                String to = coordinate[1];
                Move move = new Move(from, to);
                System.out.println(move);
                mv = mvF.raturnValidatorForPiece(board,move);
                if ((!mv.isValid(board, move))||(board.getPiece(move.getFromX(),move.getFromY()).getPieceSet()!=currentPlayer.getPieceSet())) {
                    System.out.println("Zły ruch jeszcze raz");
                } else {
                    board.performMove(move);
                    reverse = !reverse;
                    if (currentPlayer==player1){
                        currentPlayer=player2;
                    }else {
                        currentPlayer=player1;
                    }
                }

        }}
    }

}
