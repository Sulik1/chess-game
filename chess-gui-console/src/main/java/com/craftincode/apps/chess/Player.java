package com.craftincode.apps.chess;

import com.craftincode.apps.chess.piece.PieceSet;

public class Player {
    private PieceSet pieceSet;
    private String name;

    public Player(String name,PieceSet pieceSet) {
        this.pieceSet = pieceSet;
        this.name = name;
    }

    public PieceSet getPieceSet() {
        return pieceSet;
    }

    public void setPieceSet(PieceSet pieceSet) {
        this.pieceSet = pieceSet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
