package com.craftincode.apps.chess.logic.validation;

import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.logic.Move;
import com.craftincode.apps.chess.piece.PieceSet;

public class BishopMoveValidator implements MoveValidator {
    @Override
    public boolean isValid(ChessGameBoard board, Move move) {
        try {
            if (tryToMoveNullObject(board, move)) {
                return false;
            }
        }catch (NullPointerException e){
            return false;
        }

        if (staysInPlace(move)) {
            return false;
        } else if (tryToMoveForwardLeftWhite(board, move)) {
            return true;
        } else if (tryToMoveBackBlackLeft(board, move)) {
            return true;
        } else if (tryToMoveForwardRightWhite(board, move)) {
            return true;
        } else if (tryToMoveBackRightBlack(board, move)) {
            return true;
        }else if (tryToMoveWhiteLeftBack(board,move)){
            return true;
        }else if (tryToMoveBlackLeftForward(board,move)){
            return true;
        }else if (tryToMoveWhiteRightBack(board,move)){
            return true;
        }else if (tryToMoveBlackRightForward(board,move)){
            return true;
        }


        return false;
    }


    private boolean staysInPlace(Move move) {
        return move.getFromX() == move.getToX() && move.getFromY() == move.getToY();
    }

    private boolean tryToMoveNullObject(ChessGameBoard board, Move move) {
        if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == null) {
            return false;
        }

        return false;
    }


    private boolean tryToMoveForwardLeftWhite(ChessGameBoard board, Move move) {
        if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.WHITE) {
            //move left up
            if (((move.getFromX() > move.getToX()) && (move.getFromY() > move.getToY())&&(move.getFromX()-move.getToX()==move.getFromY()-move.getToY()))) {
                int cX = 1;
                int cY = 1;
                for (int i = move.getFromX() - 1; i > move.getToX(); i--) {
                    if (board.getPiece(move.getFromX() - cX, move.getFromY() - cY) != null) {
                        return false;
                    }
                    cX++;
                    cY++;
                }
                if (board.getPiece(move.getToX(), move.getToY()) == null || board.getPiece(move.getToX(), move.getToY()).getPieceSet() == PieceSet.BLACK) {
                    return true;
                } else {
                    return false;
                }

            }
        }
        return false;
    }

    private boolean tryToMoveBackBlackLeft(ChessGameBoard board, Move move) {
        if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.BLACK) {
            //move left up
            if (((move.getFromX() > move.getToX()) && (move.getFromY() > move.getToY())&&(move.getFromX()-move.getToX()==move.getFromY()-move.getToY()))) {
                int cX = 1;
                int cY = 1;
                for (int i = move.getFromX() - 1; i > move.getToX(); i--) {
                    if (board.getPiece(move.getFromX() - cX, move.getFromY() - cY) != null) {
                        return false;
                    }
                    cX++;
                    cY++;
                }
                if (board.getPiece(move.getToX(), move.getToY()) == null || board.getPiece(move.getToX(), move.getToY()).getPieceSet() == PieceSet.WHITE) {
                    return true;
                } else {
                    return false;
                }

            }
            return false;
        }
        return false;
    }

    private boolean tryToMoveForwardRightWhite(ChessGameBoard board, Move move) {
        if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.WHITE) {
            if (((move.getFromX() < move.getToX()) && (move.getFromY() > move.getToY())&&(move.getToX()-move.getFromX()==move.getFromY()-move.getToY()))) {
                int cX = 1;
                int cY = 1;
                for (int i = move.getFromX() + 1; i < move.getToX(); i++) {
                    if (board.getPiece(move.getFromX() + cX, move.getFromY() - cY) != null) {
                        return false;
                    }
                    cX++;
                    cY++;
                }
                if (board.getPiece(move.getToX(), move.getToY()) == null || board.getPiece(move.getToX(), move.getToY()).getPieceSet() == PieceSet.BLACK) {
                    return true;
                } else {
                    return false;
                }

            }
            return false;

        }
        return false;
    }

    private boolean tryToMoveBackRightBlack(ChessGameBoard board, Move move) {
        if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.BLACK) {
            if (((move.getFromX() < move.getToX()) && (move.getFromY() > move.getToY())&&(move.getToX()-move.getFromX()==move.getFromY()-move.getToY()))) {
                int cX = 1;
                int cY = 1;
                for (int i = move.getFromX() + 1; i < move.getToX(); i++) {
                    if (board.getPiece(move.getFromX() + cX, move.getFromY() - cY) != null) {
                        return false;
                    }
                    cX++;
                    cY++;
                }
                if (board.getPiece(move.getToX(), move.getToY()) == null || board.getPiece(move.getToX(), move.getToY()).getPieceSet() == PieceSet.WHITE) {
                    return true;
                } else {
                    return false;
                }

            }
            return false;

        }
        return false;
    }

    private boolean tryToMoveWhiteLeftBack(ChessGameBoard board, Move move) {
        //blackForwardLeft
        if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.WHITE) {
            if ((move.getFromX() > move.getToX()) && (move.getFromY() < move.getToY())&&(move.getFromX()+move.getFromY()==move.getToY()+move.getToX())) {
                int cX = -1;
                int cY = 1;
                for (int i = move.getFromX() - 1; i > move.getToX(); i--) {
                    if (board.getPiece(move.getFromX() + cX, move.getFromY() + cY) != null) {
                        return false;
                    }
                    cX--;
                    cY++;
                }
                if (board.getPiece(move.getToX(), move.getToY()) == null || board.getPiece(move.getToX(), move.getToY()).getPieceSet() == PieceSet.BLACK) {
                    return true;
                } else {
                    return false;


                }

            }
            return false;
        }
        return false;
    }
    private boolean tryToMoveBlackLeftForward(ChessGameBoard board, Move move) {
        //blackForwardLeft
        if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.BLACK) {
            if (((move.getFromX() > move.getToX()) && (move.getFromY() < move.getToY())&&(move.getFromY() < move.getToY()))&&((move.getFromX()+move.getFromY()==move.getToY()+move.getToX()))) {
                int cX = -1;
                int cY = 1;
                for (int i = move.getFromX() - 1; i > move.getToX(); i--) {
                    if (board.getPiece(move.getFromX() + cX, move.getFromY() + cY) != null) {
                        return false;
                    }
                    cX--;
                    cY++;
                }
                if (board.getPiece(move.getToX(), move.getToY()) == null || board.getPiece(move.getToX(), move.getToY()).getPieceSet() == PieceSet.WHITE) {
                    return true;
                } else {
                    return false;


                }

            }
            return false;
        }
        return false;
    }
    private boolean tryToMoveWhiteRightBack(ChessGameBoard board, Move move) {

        if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.WHITE) {
            if (((move.getFromX() < move.getToX()) && (move.getFromY() < move.getToY())&&(move.getFromY()-move.getFromX()==move.getToY()-move.getToX()))) {
                int cX = 1;
                int cY = 1;
                for (int i = move.getFromX() + 1; i < move.getToX(); i++) {
                    if (board.getPiece(move.getFromX() + cX, move.getFromY() + cY) != null) {
                        return false;
                    }
                    cX++;
                    cY++;
                }
                if (board.getPiece(move.getToX(), move.getToY()) == null || board.getPiece(move.getToX(), move.getToY()).getPieceSet() == PieceSet.BLACK) {
                    return true;
                } else {
                    return false;


                }

            }
            return false;
        }
        return false;
    }
    private boolean tryToMoveBlackRightForward(ChessGameBoard board, Move move) {

        if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.BLACK) {
            if (((move.getFromX() < move.getToX()) && (move.getFromY() < move.getToY())&&(move.getFromY()-move.getFromX()==move.getToY()-move.getToX()))) {
                int cX = 1;
                int cY = 1;
                for (int i = move.getFromX() + 1; i < move.getToX(); i++) {
                    if (board.getPiece(move.getFromX() + cX, move.getFromY() + cY) != null) {
                        return false;
                    }
                    cX++;
                    cY++;
                }
                if (board.getPiece(move.getToX(), move.getToY()) == null || board.getPiece(move.getToX(), move.getToY()).getPieceSet() == PieceSet.WHITE) {
                    return true;
                } else {
                    return false;


                }

            }
            return false;
        }
        return false;
    }

}



