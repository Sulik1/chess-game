package com.craftincode.apps.chess.logic;

import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.logic.validation.*;
import com.craftincode.apps.chess.piece.Piece;

public class MoveValidationFabric {
    private PawnMoveValidator pawnMoveValidator = new PawnMoveValidator();
    private RookMoveValidator rookMoveValidator = new RookMoveValidator();
    private KnightMoveValidator knightMoveValidator = new KnightMoveValidator();
    private BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
    private QueenMoveValidator queenMoveValidator= new QueenMoveValidator();
    private KingMoveValidator kingMoveValidator = new KingMoveValidator();
    private NullObjectValidator nullObjectValidator = new NullObjectValidator();

    public MoveValidator raturnValidatorForPiece(ChessGameBoard board, Move move){
       Piece piece = board.getPiece(move.getFromX(),move.getFromY());
       if (board.getPiece(move.getFromX(),move.getFromY())==null){
           return nullObjectValidator;
       }
        switch (piece.getPieceType()){
            case PAWN:
                 return pawnMoveValidator;
            case ROOK:
                return rookMoveValidator;
            case KNIGHT:
                return knightMoveValidator;
            case BISHOP:
                return bishopMoveValidator;
            case QUEEN:
                return queenMoveValidator;
            case KING:
                return kingMoveValidator;


        }
        return null;

    }
}
