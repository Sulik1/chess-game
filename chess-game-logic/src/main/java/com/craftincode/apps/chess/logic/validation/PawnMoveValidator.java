package com.craftincode.apps.chess.logic.validation;

import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.logic.Move;
import com.craftincode.apps.chess.piece.Piece;
import com.craftincode.apps.chess.piece.PieceSet;

public class PawnMoveValidator implements MoveValidator {

    @Override
    public  boolean isValid(ChessGameBoard gameBoard, Move move) {
        Piece piece = gameBoard.getPiece(move.getFromX(),move.getFromY());
        Piece opponent = gameBoard.getPiece(move.getToX(),move.getToY());
//        PieceSet set = piece.getPieceSet();
        try {
            if (tryToMoveNullObject(gameBoard, move)) {
                return false;
            }
        }catch (NullPointerException e){
            return false;
        }

        if(staysInPlace(move)) {
            return false;
        }

        //Normal move
        else if(isMoveForwardNSteps(1,move,piece.getPieceSet()) && opponent == null ){
            return true;
        }

        //First move
        else if((Math.abs(move.getFromY() - move.getToY())==2) && Math.abs(move.getFromX() - move.getToX())==0 && (move.getFromY() ==1 || move.getFromY() ==6) && opponent == null) {
            int forwardMoveModifier = piece.getPieceSet().equals(PieceSet.BLACK) ? 1 : -1;
            if(gameBoard.getPiece(move.getFromX(),move.getFromY()+(1*forwardMoveModifier))!= null) return false;
            if ((piece.getPieceSet() == PieceSet.WHITE)) {
                if (move.getFromY() > move.getToY()) {
                    return true;
                }
            }
            if ((piece.getPieceSet() == PieceSet.BLACK)) {
                if (move.getFromY() < move.getToY()) {
                    return true;
                }
            }
        }
        //Beat
        else  if ((Math.abs(move.getFromY() - move.getToY())==1) && ((move.getFromX() - move.getToX())== 1 || (move.getFromX() - move.getToX())== -1)&& opponent != null) {
            if((piece.getPieceSet()==PieceSet.WHITE && opponent.getPieceSet().equals(PieceSet.BLACK))){
                if(move.getFromY()>move.getToY()){
                    return true;
                }
            }
            if((piece.getPieceSet()==PieceSet.BLACK && opponent.getPieceSet().equals(PieceSet.WHITE))){
                if(move.getFromY()<move.getToY()){
                    return true;
                }


            }

        }
        return false;
    }

    private boolean isMoveForwardNSteps(int stepsToCheck, Move move, PieceSet setOfPieceToBeMoved){
        int forwardMoveModifier = setOfPieceToBeMoved.equals(PieceSet.BLACK) ? 1 : -1;
        return (move.verticalShift() * forwardMoveModifier ==stepsToCheck)  &&
                move.horizontalShift()==0;
    }

    private boolean staysInPlace(Move move) {
        return move.getFromX() == move.getToX() && move.getFromY() == move.getToY();
    }
    private boolean tryToMoveNullObject(ChessGameBoard board, Move move){
        if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == null) {
            return false;
        } else if (staysInPlace(move)) {
            return false;
        }
        return false;
    }


}

