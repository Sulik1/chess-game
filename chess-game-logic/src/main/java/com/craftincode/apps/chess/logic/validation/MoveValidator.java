package com.craftincode.apps.chess.logic.validation;

import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.logic.Move;

public interface MoveValidator {
    boolean isValid(ChessGameBoard board, Move move);
}
