package com.craftincode.apps.chess.logic.validation;

import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.logic.Move;
import com.craftincode.apps.chess.piece.Piece;
import com.craftincode.apps.chess.piece.PieceSet;

public class RookMoveValidator implements MoveValidator {

    @Override
    public boolean isValid(ChessGameBoard board, Move move) {

       try {
           if (tryToMoveNullObject(board, move)) {
               return false;
           }
       }catch (NullPointerException e){
           return false;
       }

        if (staysInPlace(move)) {
            return false;
        } else if (trytoMoveForward(board, move)) {
            return true;
        } else if (tryToMoveBack(board, move)) {
            return true;
        }else if (tryToMoveRight(board,move)){
            return true;
        }else if (tryToMoveLeft(board,move)){
            return true;
        }

        return false;
    }


    private boolean staysInPlace(Move move) {
        return move.getFromX() == move.getToX() && move.getFromY() == move.getToY();
    }

    private boolean tryToMoveNullObject(ChessGameBoard board, Move move) {
        if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == null) {
            return true;
        } else if (staysInPlace(move)) {
            return false;
        }
        return false;
    }

    private boolean trytoMoveForward(ChessGameBoard board, Move move) {

        if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.WHITE) {
            if (move.getFromX() == move.getToX() && move.getToY() < move.getFromY()) {
                for (int i = move.getFromY() - 1; i > move.getToY(); i--) {
                    if (board.getPiece(move.getFromX(), i) != null) {
                        return false;
                    }

                }
                if (board.getPiece(move.getToX(), move.getToY()) == null || board.getPiece(move.getToX(), move.getToY()).getPieceSet() == PieceSet.BLACK) {
                    return true;
                } else {
                    return false;
                }

            }
        }else if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.BLACK) {
            if (move.getFromX() == move.getToX() && move.getFromY() < move.getToY()) {
                for (int i = move.getFromY() + 1; i < move.getToY(); i++) {
                    if (board.getPiece(move.getFromX(), i) != null) {
                        return false;
                    }

                }
                if (board.getPiece(move.getToX(), move.getToY()) == null || board.getPiece(move.getToX(), move.getToY()).getPieceSet() == PieceSet.WHITE) {
                    return true;
                } else {
                    return false;
                }

            }else
            return false;
        }
        return false;

    }

    private boolean tryToMoveBack(ChessGameBoard board, Move move) {
        if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.WHITE) {
            if (move.getFromX() == move.getToX() && move.getFromY() < move.getToY()) {
                for (int i = move.getFromY() + 1; i < move.getToY(); i++) {
                    if (board.getPiece(move.getFromX(), i) != null) {
                        return false;
                    }

                }
                if (board.getPiece(move.getToX(), move.getToY()) == null || board.getPiece(move.getToX(), move.getToY()).getPieceSet() == PieceSet.BLACK) {
                    return true;
                } else {
                    return false;
                }

            }
        }else if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.BLACK){
            if (move.getFromX() == move.getToX() && move.getToY() < move.getFromY()) {
                for (int i = move.getFromY() - 1; i > move.getToY(); i--) {
                    if (board.getPiece(move.getFromX(), i) != null) {
                        return false;
                    }

                }
                if (board.getPiece(move.getToX(), move.getToY()) == null || board.getPiece(move.getToX(), move.getToY()).getPieceSet() == PieceSet.WHITE) {
                    return true;
                } else {
                    return false;
                }

            }else
            return false;
        }
        return false;

    }

    private boolean tryToMoveLeft(ChessGameBoard board, Move move) {
        if ( board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.WHITE) {
            if (move.getFromY() == move.getToY() && move.getToX() < move.getFromX()) {
                for (int i = move.getFromX() - 1; i > move.getToX(); i--) {
                    if (board.getPiece(i, move.getFromY()) != null) {
                        return false;
                    }
                }
                if (board.getPiece(move.getToX(), move.getToY()) == null || board.getPiece(move.getToX(), move.getToY()).getPieceSet() == PieceSet.BLACK) {
                    return true;
                } else {
                    return false;
                }
            }
        }else if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.BLACK){
            if (move.getFromY() == move.getToY() && move.getToX() < move.getFromX()) {
                for (int i = move.getFromX() - 1; i > move.getToX(); i--) {
                    if (board.getPiece(i, move.getFromY()) != null) {
                        return false;
                    }
                }
                if (board.getPiece(move.getToX(), move.getToY()) == null || board.getPiece(move.getToX(), move.getToY()).getPieceSet() == PieceSet.WHITE) {
                    return true;
                } else {
                    return false;
                }
            }else
            return false;
        }
        return false;
    }

    private boolean tryToMoveRight(ChessGameBoard board, Move move) {
        if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.WHITE) {
            if (move.getFromY() == move.getToY() && move.getToX() > move.getFromX()) {
                for (int i = move.getFromX() + 1; i < move.getToX(); i++) {
                    if (board.getPiece(i, move.getFromY()) != null) {
                        return false;
                    }
                }
                if (board.getPiece(move.getToX(), move.getToY()) == null || board.getPiece(move.getToX(), move.getToY()).getPieceSet() == PieceSet.BLACK) {
                    return true;
                } else {
                    return false;
                }

            }
        }else if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.BLACK){
            if (move.getFromY() == move.getToY() && move.getToX() > move.getFromX()) {
                for (int i = move.getFromX() + 1; i < move.getToX(); i++) {
                    if (board.getPiece(i, move.getFromY()) != null) {
                        return false;
                    }
                }
                if (board.getPiece(move.getToX(), move.getToY()) == null || board.getPiece(move.getToX(), move.getToY()).getPieceSet() == PieceSet.WHITE) {
                    return true;
                } else {
                    return false;
                }

            }else
            return false;
        }
        return false;

    }


}

