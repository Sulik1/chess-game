package com.craftincode.apps.chess.logic.validation;

import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.logic.Move;
import com.craftincode.apps.chess.logic.MoveValidationFabric;
import com.craftincode.apps.chess.piece.PieceSet;

public class KingMoveValidator implements MoveValidator {
    @Override
    public boolean isValid(ChessGameBoard board, Move move) {

        try {
            if (tryToMoveNullObject(board, move)) {
                return false;

            }
        }catch (NullPointerException e){
            return false;
        }

             if (staysInPlace(move)) {
                return false;
            } else if (moveKing(board, move) && isPlaceWhereYouWantToGoIsSafe(board, move)) {
                return true;
            }
            return false;

    }


    private boolean staysInPlace(Move move) {
        return move.getFromX() == move.getToX() && move.getFromY() == move.getToY();
    }

    private boolean tryToMoveNullObject(ChessGameBoard board, Move move) {
        if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == null) {
            return false;
        }

        return false;
    }

    private boolean moveKing(ChessGameBoard board,Move move) {

        if ((move.getToX() < 8 && move.getToY() < 8) && (move.getToY() >= 0 && move.getToX() >= 0) && (move.getFromY() < 8 && move.getFromX() < 8) && (move.getFromY() >= 0 && move.getFromX() >= 0)) {
            if ((move.getFromX() == move.getToX()) || (move.getFromX() == move.getToX() - 1) || (move.getFromX() == move.getToX() + 1)) {
                if ((move.getFromY() == move.getToY()) || (move.getFromY() == move.getToY() - 1) || (move.getFromY() == move.getToY() + 1)) {

                    if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.WHITE) {
                        if ((board.getPiece(move.getToX(), move.getToY()) == null || board.getPiece(move.getToX(), move.getToY()).getPieceSet() == PieceSet.BLACK)) {
                            return true;
                        } else {
                            return false;
                        }

                    } else if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.BLACK) {
                        if ((board.getPiece(move.getToX(), move.getToY()) == null || board.getPiece(move.getToX(), move.getToY()).getPieceSet() == PieceSet.WHITE)) {
                            return true;
                        } else {
                            return false;
                        }
                    }

                }
                return false;
            }
            return false;
        }
        return false;
    }
//testy tej metody!!
    public boolean isPlaceWhereYouWantToGoIsSafe(ChessGameBoard board, Move move) {
        MoveValidationFabric mvF = new MoveValidationFabric();
        MoveValidator mv;
        Move move1;
        board.setPiece(move.getToX(),move.getToY(),board.getPiece(move.getFromX(),move.getFromY()));
        board.setPiece(move.getFromX(),move.getFromY(),null);
        String convertAlaPositionKing = ConvertPossitionToString(move.getToX(),move.getToY());
            for (char k = 65; k <= 72; k++) {
                for (char h = 49; h < 57; h++) {
                    StringBuilder sc = new StringBuilder();
                    sc.append(k).append(h);
                    String from = sc.toString();
                    move1 = new Move(from,convertAlaPositionKing);
                    mv = mvF.raturnValidatorForPiece(board, move1);
                    if ((mv.isValid(board,move1))) {
                        return false;
                    }
                }
            }
        board.setPiece(move.getFromX(),move.getFromY(),board.getPiece(move.getToX(),move.getToY()));
            board.setPiece(move.getToX(),move.getToY(),null);
        return true;
    }


    private String ConvertPossitionToString(int x, int y) {
        int xs = (char) x + 65;
        int ys = (char) (56 - y);

        char xsss = (char) xs;
        char ysss = (char) ys;
        StringBuilder sc = new StringBuilder();
        sc.append(xsss).append(ysss);
        String result = sc.toString();
        return result;
    }
    private   int convertX(char coordinateX) {
        switch (coordinateX) {
            case 'A':
                return 0;
            case 'B':
                return 1;
            case 'C':
                return 2;
            case 'D':
                return 3;
            case 'E':
                return 4;
            case 'F':
                return 5;
            case 'G':
                return 6;
            case 'H':
                return 7;
            default:
                return 100;
        }
    }
    private int convertY(char coordinateY) {
        return (int) coordinateY - '0' - 1;
    }
}
