package com.craftincode.apps.chess.piece;

public enum PieceType {
    QUEEN,KING,KNIGHT,BISHOP,PAWN,ROOK
}
