package com.craftincode.apps.chess.logic;

public class Move {
    private int fromX,fromY,toX,toY;

    public Move(String from , String to){
        char[] fromChars = from.toUpperCase().toCharArray();
        char letterCord = fromChars[0];
        fromX =letterCord-65;
        fromY =8 - Integer.parseInt(String.valueOf(fromChars[1]));

        char[] toChars = to.toUpperCase().toCharArray();
        char letterCords = toChars[0];
        toX =letterCords-65;
        toY =8 - Integer.parseInt(String.valueOf(toChars[1]));
    }
    int convertY(char coordinateX){
        switch(coordinateX){
            case 'A' :
                return 0;
            case 'B':
                return 1;
            case 'C':
                return 2;
            case 'D':
                return 3;
            case 'E':
                return 4;
            case 'F':
                return 5;
            case 'G':
                return 6;
            case 'H':
                return 7;
            default:
                return 100;
        }
    }
    int convertX(char coordinateY){
        return (int) coordinateY-'0'-1;
    }


    public int getFromX() {
        return fromX;
    }

    public int getFromY() {
        return fromY;
    }

    public int getToX() {
        return toX;
    }

    public int getToY() {
        return toY;
    }

    public int verticalShift(){
        return toY - fromY;
    }

    public int horizontalShift(){
        return toX - fromX;
    }

    @Override
    public String toString() {
        return "Move{" +
                "fromX=" + fromX +
                ", fromY=" + fromY +
                ", toX=" + toX +
                ", toY=" + toY +
                '}';
    }
}
