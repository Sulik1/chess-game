package com.craftincode.apps.chess.logic;

import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.logic.validation.KingMoveValidator;
import com.craftincode.apps.chess.logic.validation.MoveValidator;
import com.craftincode.apps.chess.piece.Piece;
import com.craftincode.apps.chess.piece.PieceSet;
import com.craftincode.apps.chess.piece.PieceType;

import java.util.*;

public class CheckOrCheckMate {

    public boolean isCheck(ChessGameBoard board) {
        if (KingCheck(board)) {
            return true;
        }
        return false;
    }

    public boolean isCheckMate(ChessGameBoard board, boolean player) {
        if (!canYourKingMoveAnyWhere(board, player) && !canYouSafeYourKingByBeatingSomeoneWhoCanBeatKing(board, player)) {
            return true;
        }
        return false;
    }

    //normalnie ma byc private
    public boolean canYourKingMoveAnyWhere(ChessGameBoard board, boolean playerWhite) {
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        //todo jak moze sie ruszyc to return true a jak nie to false
        int YPos = 10;
        int XPos = 10;
        int YPosB = 10;
        int XPosB = 10;
        boolean result = false;
        //where is your king
        for (int i = 0; i < board.getBoard().length; i++) {
            for (int j = 0; j < board.getBoard().length; j++) {
                if (board.getPiece(i, j) == null) {
                    continue;

                } else if ((board.getPiece(i, j).getPieceType() == PieceType.KING) && (board.getPiece(i, j).getPieceSet() == PieceSet.WHITE)) {
                    XPos = i;
                    YPos = j;
                } else if ((board.getPiece(i, j).getPieceType() == PieceType.KING) && (board.getPiece(i, j).getPieceSet() == PieceSet.BLACK)) {
                    XPosB = i;
                    YPosB = j;
                }
            }
        }
        if (playerWhite) {
            Map<Integer, List<Integer>> map = allPosPlaceToMoveForKing(XPos, YPos);
            board.setPiece(XPos, YPos, null);
            for (int i = 0; i < 8; i++) {
                List<Integer> pos = map.get(i);
                if ((pos.get(0) >= 0 && pos.get(0) < 8) && (pos.get(1) >= 0 && pos.get(1) < 8)) {
                    if (board.getPiece(pos.get(0), pos.get(1)) == null || board.getPiece(pos.get(0), pos.get(1)).getPieceSet() == PieceSet.BLACK) {
                        board.setPiece(pos.get(0), pos.get(1), new Piece(PieceType.KING, PieceSet.WHITE));
                        if (!KingCheck(board)) {
                            return true;
                        } else {
                            board.setPiece(pos.get(0), pos.get(1), null);
                        }
                    }
                }
            }

        } else {
            Map<Integer, List<Integer>> map = allPosPlaceToMoveForKing(XPosB, YPosB);
            board.setPiece(XPosB, YPosB, null);
            for (int i = 0; i < 8; i++) {
                List<Integer> pos = map.get(i);
                if ((pos.get(0) > 0 && pos.get(0) < 8) && (pos.get(1) > 0 && pos.get(1) < 8)) {
                    if (board.getPiece(pos.get(0), pos.get(1)) == null || board.getPiece(pos.get(0), pos.get(1)).getPieceSet() == PieceSet.WHITE) {
                        board.setPiece(pos.get(0), pos.get(1), new Piece(PieceType.KING, PieceSet.BLACK));
                        if (!KingCheck(board)) {
                            return true;
                        } else {
                            board.setPiece(pos.get(0), pos.get(1), null);
                        }
                    }
                }
            }


        }
        board.setPiece(XPos,YPos,new Piece(PieceType.KING,PieceSet.WHITE));
        board.setPiece(XPosB,YPosB,new Piece(PieceType.KING,PieceSet.BLACK));
        return false;
    }

    //normalnie ma byc private
    public boolean canYouSafeYourKingByBeatingSomeoneWhoCanBeatKing(ChessGameBoard board, boolean playerWhite) {
        Move move;
        MoveValidationFabric mvF = new MoveValidationFabric();
        MoveValidator mv;

            String opPos = whereIsThatPersonWhoCanBeatKing(board);
            if (opPos.equals("toMuchBeats")){
                return false;
            }else {
                for (char k = 65; k <= 72; k++) {
                    for (char h = 49; h < 57; h++) {
                        StringBuilder sc = new StringBuilder();
                        sc.append(k).append(h);
                        String from = sc.toString();
                        move = new Move(from, opPos);
                        mv = mvF.raturnValidatorForPiece(board, move);
                        if ((mv.isValid(board, move))) {
                            return true;
                        }
                    }
                }
            }

            return false;

    }


    private String ConvertPossitionToString(int x, int y) {
        int xs = (char) x + 65;
        int ys = (char) (56 - y);

        char xsss = (char) xs;
        char ysss = (char) ys;
        StringBuilder sc = new StringBuilder();
        sc.append(xsss).append(ysss);
        String result = sc.toString();
        return result;
    }

    private boolean KingCheck(ChessGameBoard board) {
        MoveValidationFabric mvF = new MoveValidationFabric();
        MoveValidator mv;
        MoveValidator mv1;
        Move move;
        Move move1;
        int YPos = 10;
        int XPos = 10;
        int YPosB = 10;
        int XPosB = 10;
        //where is your White king
        for (int i = 0; i < board.getBoard().length; i++) {
            for (int j = 0; j < board.getBoard().length; j++) {
                if (board.getPiece(i, j) == null) {
                    continue;

                } else if ((board.getPiece(i, j).getPieceType() == PieceType.KING) && (board.getPiece(i, j).getPieceSet() == PieceSet.WHITE)) {
                    XPos = i;
                    YPos = j;
                } else if ((board.getPiece(i, j).getPieceType() == PieceType.KING) && (board.getPiece(i, j).getPieceSet() == PieceSet.BLACK)) {
                    XPosB = i;
                    YPosB = j;
                }
            }
        }
        //can someona beat kings
        String toWhiteKing = ConvertPossitionToString(XPos, YPos);
        String toBlackKing = ConvertPossitionToString(XPosB, YPosB);
        for (char k = 65; k <= 72; k++) {
            for (char h = 49; h < 57; h++) {
                StringBuilder sc = new StringBuilder();
                sc.append(k).append(h);
                String from = sc.toString();
                move = new Move(from, toWhiteKing);
                move1 = new Move(from, toBlackKing);
                mv = mvF.raturnValidatorForPiece(board, move);
                mv1 = mvF.raturnValidatorForPiece(board, move1);
                if ((mv.isValid(board, move)) || (mv1.isValid(board, move1))) {
                    return true;
                }
            }
        }
        return false;


    }

    private Map<Integer, List<Integer>> allPosPlaceToMoveForKing(int a, int b) {
        Map<Integer, List<Integer>> map = new HashMap<>();
        List<Integer> list = new LinkedList<>();
        list.add(a - 1);
        list.add(b);
        List<Integer> list1 = new LinkedList<>();
        list1.add(a + 1);
        list1.add(b);
        List<Integer> list2 = new LinkedList<>();
        list2.add(a);
        list2.add(b + 1);
        List<Integer> list3 = new LinkedList<>();
        list3.add(a);
        list3.add(b - 1);
        List<Integer> list4 = new LinkedList<>();
        list4.add(a + 1);
        list4.add(b + 1);
        List<Integer> list5 = new LinkedList<>();
        list5.add(a + 1);
        list5.add(b - 1);
        List<Integer> list6 = new LinkedList<>();
        list6.add(a - 1);
        list6.add(b - 1);
        List<Integer> list7 = new LinkedList<>();
        list7.add(a - 1);
        list7.add(b + 1);

        map.put(0, list);
        map.put(1, list1);
        map.put(2, list2);
        map.put(3, list3);
        map.put(4, list4);
        map.put(5, list5);
        map.put(6, list6);
        map.put(7, list7);
        return map;


    }



    private String whereIsThatPersonWhoCanBeatKing(ChessGameBoard board) {
        MoveValidationFabric mvF = new MoveValidationFabric();
        MoveValidator mv;
        MoveValidator mv1;
        Move move;
        Move move1;
        String opponetPos = "";
        int YPos = 10;
        int XPos = 10;
        int YPosB = 10;
        int XPosB = 10;
        int counter = 0;
        //where is your White king
        for (int i = 0; i < board.getBoard().length; i++) {
            for (int j = 0; j < board.getBoard().length; j++) {
                if (board.getPiece(i, j) == null) {
                    continue;

                } else if ((board.getPiece(i, j).getPieceType() == PieceType.KING) && (board.getPiece(i, j).getPieceSet() == PieceSet.WHITE)) {
                    XPos = i;
                    YPos = j;
                } else if ((board.getPiece(i, j).getPieceType() == PieceType.KING) && (board.getPiece(i, j).getPieceSet() == PieceSet.BLACK)) {
                    XPosB = i;
                    YPosB = j;
                }
            }
        }
        //can someona beat kings
        String toWhiteKing = ConvertPossitionToString(XPos, YPos);
        String toBlackKing = ConvertPossitionToString(XPosB, YPosB);
        for (char k = 65; k <= 72; k++) {
            for (char h = 49; h < 57; h++) {
                StringBuilder sc = new StringBuilder();
                sc.append(k).append(h);
                String from = sc.toString();
                move = new Move(from, toWhiteKing);
                move1 = new Move(from, toBlackKing);
                mv = mvF.raturnValidatorForPiece(board, move);
                mv1 = mvF.raturnValidatorForPiece(board, move1);
                if ((mv.isValid(board, move)) || (mv1.isValid(board, move1))) {
                    counter++;
                    opponetPos = from;
                }
            }
        }
        if (counter==1) {
            return opponetPos;
        }else {
            return "toMuchBeats";
        }

    }
    public String whereIsKing(ChessGameBoard board, boolean playerWhite){
        int YPos = 10;
        int XPos = 10;
        int YPosB = 10;
        int XPosB = 10;
        //where is your White king
        for (int i = 0; i < board.getBoard().length; i++) {
            for (int j = 0; j < board.getBoard().length; j++) {
                if (board.getPiece(i, j) == null) {
                    continue;

                } else if ((board.getPiece(i, j).getPieceType() == PieceType.KING) && (board.getPiece(i, j).getPieceSet() == PieceSet.WHITE)) {
                    XPos = i;
                    YPos = j;
                } else if ((board.getPiece(i, j).getPieceType() == PieceType.KING) && (board.getPiece(i, j).getPieceSet() == PieceSet.BLACK)) {
                    XPosB = i;
                    YPosB = j;
                }
            }
        }
        if (playerWhite){
            return ConvertPossitionToString(XPos,YPos);
        }else {
            return ConvertPossitionToString(XPosB,YPosB);
        }
    }

}
