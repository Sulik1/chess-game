package com.craftincode.apps.chess.logic.validation;

import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.logic.Move;

public class NullObjectValidator implements MoveValidator {
    @Override
    public boolean isValid(ChessGameBoard board, Move move) {
        return false;
    }
}
