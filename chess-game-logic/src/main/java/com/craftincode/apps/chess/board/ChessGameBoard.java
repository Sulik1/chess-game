package com.craftincode.apps.chess.board;

import com.craftincode.apps.chess.logic.Move;
import com.craftincode.apps.chess.piece.Piece;
import com.craftincode.apps.chess.piece.PieceSet;
import com.craftincode.apps.chess.piece.PieceType;

public class ChessGameBoard {
    private Piece[][] board = initializeDefaultBoard();


    public ChessGameBoard(){}

    public ChessGameBoard(Piece[][] board) {
        this.board = board;
    }

    public void performMove(Move move){
        Piece piece = getPiece(move.getFromX(),move.getFromY());
        setPiece(move.getFromX(),move.getFromY(),null);
        setPiece(move.getToX(),move.getToY(),piece);
    }

    public Piece getPiece(int x, int y){
       return board[y][x];
    }

    public void setPiece(int x,int y, Piece piece){
        board[y][x]=piece;
    }


    private Piece[][] initializeDefaultBoard() {
        Piece[][] board = new Piece[][]{
                {new Piece(PieceType.ROOK, PieceSet.BLACK), new Piece(PieceType.KNIGHT, PieceSet.BLACK), new Piece(PieceType.BISHOP, PieceSet.BLACK), new Piece(PieceType.QUEEN, PieceSet.BLACK),
                        new Piece(PieceType.KING, PieceSet.BLACK), new Piece(PieceType.BISHOP, PieceSet.BLACK), new Piece(PieceType.KNIGHT, PieceSet.BLACK), new Piece(PieceType.ROOK, PieceSet.BLACK)},
                {new Piece(PieceType.PAWN, PieceSet.BLACK), new Piece(PieceType.PAWN, PieceSet.BLACK), new Piece(PieceType.PAWN, PieceSet.BLACK), new Piece(PieceType.PAWN, PieceSet.BLACK), new Piece(PieceType.PAWN, PieceSet.BLACK),
                        new Piece(PieceType.PAWN, PieceSet.BLACK), new Piece(PieceType.PAWN, PieceSet.BLACK), new Piece(PieceType.PAWN, PieceSet.BLACK), new Piece(PieceType.PAWN, PieceSet.BLACK)},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {new Piece(PieceType.PAWN, PieceSet.WHITE), new Piece(PieceType.PAWN, PieceSet.WHITE), new Piece(PieceType.PAWN, PieceSet.WHITE), new Piece(PieceType.PAWN, PieceSet.WHITE),
                        new Piece(PieceType.PAWN, PieceSet.WHITE), new Piece(PieceType.PAWN, PieceSet.WHITE), new Piece(PieceType.PAWN, PieceSet.WHITE), new Piece(PieceType.PAWN, PieceSet.WHITE)},
                {new Piece(PieceType.ROOK, PieceSet.WHITE), new Piece(PieceType.KNIGHT, PieceSet.WHITE), new Piece(PieceType.BISHOP, PieceSet.WHITE), new Piece(PieceType.QUEEN, PieceSet.WHITE),
                        new Piece(PieceType.KING, PieceSet.WHITE), new Piece(PieceType.BISHOP, PieceSet.WHITE), new Piece(PieceType.KNIGHT, PieceSet.WHITE), new Piece(PieceType.ROOK, PieceSet.WHITE)},
        };
        return board;
    }

    public Piece[][] getBoard() {
        return board;
    }
}
