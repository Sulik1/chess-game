package com.craftincode.apps.chess.piece;

public class Piece {
    private PieceType pieceType;
    private PieceSet pieceSet;

    public Piece(PieceType pieceType, PieceSet pieceSet) {
        this.pieceType = pieceType;
        this.pieceSet = pieceSet;
    }

    public PieceSet getPieceSet() {
        return pieceSet;
    }

    public void setPieceSet(PieceSet pieceSet) {
        this.pieceSet = pieceSet;
    }

    public PieceType getPieceType() {
        return pieceType;
    }

    public void setPieceType(PieceType pieceType) {
        this.pieceType = pieceType;
    }
}
