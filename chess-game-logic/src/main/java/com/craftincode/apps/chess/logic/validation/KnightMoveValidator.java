package com.craftincode.apps.chess.logic.validation;

import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.logic.Move;
import com.craftincode.apps.chess.piece.PieceSet;

import javax.lang.model.type.NullType;

public class KnightMoveValidator implements MoveValidator {
    @Override
    public boolean isValid(ChessGameBoard board, Move move) {
        try {
            if (tryToMoveNullObject(board, move)) {
                return false;
            }
        }catch (NullPointerException e){
            return false;
        }

        if (staysInPlace(move)) {
            return false;
        } else if (tryToMoveOutOfBoard(move)) {
            return false;
        }else if (tryToMoveForwardLong(board,move)){
            return true;
        }else if (tryToMoveBackLong(board,move)){
            return true;
        }else if (tryToMoveForwardShort(board,move)){
            return true;
        }else if (tryToMoveBackShort(board,move)){
            return true;
        }

        return false;
    }


    private boolean staysInPlace(Move move) {
        return move.getFromX() == move.getToX() && move.getFromY() == move.getToY();
    }

    private boolean tryToMoveNullObject(ChessGameBoard board, Move move) {
        if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == null) {
            return false;
        }

        return false;
    }

    private boolean tryToMoveOutOfBoard(Move move) {
        return move.getToY() > 7 || move.getToX() > 7;
    }

    private boolean tryToMoveForwardLong(ChessGameBoard board, Move move) {
        if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.WHITE) {
            if (board.getPiece(move.getToX(),move.getToY()) == null || board.getPiece(move.getToX(),move.getToY()).getPieceSet()==PieceSet.BLACK){
                if ((move.getToX()==move.getFromX()-1||move.getToX()==move.getFromX()+1)&&move.getFromY()-move.getToY()==2){
                    return true;
                }
            }

        } else if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.BLACK) {
            if (board.getPiece(move.getToX(),move.getToY())==null||board.getPiece(move.getToX(),move.getToY()).getPieceSet()==PieceSet.WHITE){
                if ((move.getToX()==move.getFromX()-1||move.getToX()==move.getFromX()+1)&&move.getToY()-move.getFromY()==2){
                    return true;
                }

            }

        }
        return false;
    }

    private boolean tryToMoveBackLong(ChessGameBoard board, Move move){
        if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.WHITE) {
            if (board.getPiece(move.getToX(),move.getToY())==null||board.getPiece(move.getToX(),move.getToY()).getPieceSet()==PieceSet.BLACK){
                if ((move.getToX()==move.getFromX()-1||move.getToX()==move.getFromX()+1)&&move.getToY()-move.getFromY()==2){
                    return true;
                }

            }

        }else if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.BLACK){
            if (board.getPiece(move.getToX(),move.getToY())==null||board.getPiece(move.getToX(),move.getToY()).getPieceSet()==PieceSet.WHITE){
                if ((move.getToX()==move.getFromX()-1||move.getToX()==move.getFromX()+1)&&move.getFromY()-move.getToY()==2){
                    return true;
                }
            }
        }

        return false;
    }

    private boolean tryToMoveForwardShort(ChessGameBoard board , Move move){
        if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.WHITE) {
            if (board.getPiece(move.getToX(),move.getToY())==null||board.getPiece(move.getToX(),move.getToY()).getPieceSet()==PieceSet.BLACK){
                if ((move.getToX()==move.getFromX()-2||move.getToX()==move.getFromX()+2)&&move.getFromY()-move.getToY()==1){
                    return true;
                }
            }

        } else if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.BLACK) {
            if (board.getPiece(move.getToX(),move.getToY())==null||board.getPiece(move.getToX(),move.getToY()).getPieceSet()==PieceSet.WHITE){
                if ((move.getToX()==move.getFromX()-2||move.getToX()==move.getFromX()+2)&&move.getToY()-move.getFromY()==1){
                    return true;
                }

            }

        }
        return false;
    }

    private boolean tryToMoveBackShort(ChessGameBoard board ,Move move){
        if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.BLACK) {
            if (board.getPiece(move.getToX(),move.getToY())==null||board.getPiece(move.getToX(),move.getToY()).getPieceSet()==PieceSet.WHITE){
                if ((move.getToX()==move.getFromX()-2||move.getToX()==move.getFromX()+2)&&move.getFromY()-move.getToY()==1){
                    return true;
                }
            }

        } else if (board.getPiece(move.getFromX(), move.getFromY()).getPieceSet() == PieceSet.WHITE) {
            if (board.getPiece(move.getToX(),move.getToY())==null||board.getPiece(move.getToX(),move.getToY()).getPieceSet()==PieceSet.BLACK){
                if ((move.getToX()==move.getFromX()-2||move.getToX()==move.getFromX()+2)&&move.getToY()-move.getFromY()==1){
                    return true;
                }

            }

        }
        return false;
    }
}
