package com.craftincode.apps.chess.logic.validation;

import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.logic.Move;

public class QueenMoveValidator implements MoveValidator {
    @Override
    public boolean isValid(ChessGameBoard board, Move move) {
        BishopMoveValidator bishopMoveValidator=new BishopMoveValidator();
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        if (bishopMoveValidator.isValid(board,move)||rookMoveValidator.isValid(board,move)){
            return true;
        }else {
            return false;
        }
    }
}
