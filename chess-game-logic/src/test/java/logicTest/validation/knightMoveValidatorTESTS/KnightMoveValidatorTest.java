package logicTest.validation.knightMoveValidatorTESTS;

import com.craftincode.apps.chess.logic.validation.KnightMoveValidator;
import org.junit.Test;
import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.logic.Move;
import com.craftincode.apps.chess.piece.Piece;
import com.craftincode.apps.chess.piece.PieceSet;
import com.craftincode.apps.chess.piece.PieceType;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class KnightMoveValidatorTest {
    @Test
    public void tryToMoveWhiteForwardLeftClearWay(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,5,new Piece(PieceType.KNIGHT,PieceSet.WHITE));
        Move move = new Move("C3","B5");
        KnightMoveValidator knightMoveValidator = new KnightMoveValidator();
        boolean result = knightMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveBlackForwardLeftClearWay(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,2,new Piece(PieceType.KNIGHT,PieceSet.BLACK));
        Move move = new Move("C6","D4");
        KnightMoveValidator knightMoveValidator = new KnightMoveValidator();
        boolean result = knightMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveBlackForwardRighBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,2,new Piece(PieceType.KNIGHT,PieceSet.BLACK));
        board.setPiece(3,4,new Piece(PieceType.KNIGHT,PieceSet.WHITE));
        Move move = new Move("C6","D4");
        KnightMoveValidator knightMoveValidator = new KnightMoveValidator();
        boolean result = knightMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveBlackForwardRighBeatOWN(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,2,new Piece(PieceType.KNIGHT,PieceSet.BLACK));
        board.setPiece(3,4,new Piece(PieceType.PAWN,PieceSet.BLACK));
        Move move = new Move("C6","D4");
        KnightMoveValidator knightMoveValidator = new KnightMoveValidator();
        boolean result = knightMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveWhiteForwardLeftBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,2,new Piece(PieceType.KNIGHT,PieceSet.WHITE));
        board.setPiece(3,4,new Piece(PieceType.KNIGHT,PieceSet.BLACK));
        Move move = new Move("C6","D4");
        KnightMoveValidator knightMoveValidator = new KnightMoveValidator();
        boolean result = knightMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveWhiteForwardRighBeatOWN(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,2,new Piece(PieceType.KNIGHT,PieceSet.WHITE));
        board.setPiece(3,4,new Piece(PieceType.PAWN,PieceSet.WHITE));
        Move move = new Move("C6","D4");
        KnightMoveValidator knightMoveValidator = new KnightMoveValidator();
        boolean result = knightMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveWhiteForwardNoClearWay(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,2,new Piece(PieceType.KNIGHT,PieceSet.WHITE));
        board.setPiece(2,3,new Piece(PieceType.PAWN,PieceSet.WHITE));
        Move move = new Move("C6","D4");
        KnightMoveValidator knightMoveValidator = new KnightMoveValidator();
        boolean result = knightMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveWhiteForwardSHORTRIGHTClearWay(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,5,new Piece(PieceType.KNIGHT,PieceSet.WHITE));
        Move move = new Move("C3","E4");
        KnightMoveValidator knightMoveValidator = new KnightMoveValidator();
        boolean result = knightMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveWhiteForwardSHORTLEFTClearWay(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,5,new Piece(PieceType.KNIGHT,PieceSet.WHITE));
        Move move = new Move("C3","A4");
        KnightMoveValidator knightMoveValidator = new KnightMoveValidator();
        boolean result = knightMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveWhiteBackSHORTrightClearWay(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,3,new Piece(PieceType.KNIGHT,PieceSet.WHITE));
        Move move = new Move("C5","E4");
        KnightMoveValidator knightMoveValidator = new KnightMoveValidator();
        boolean result = knightMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveWhiteBackSHORTleftClearWay(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,3,new Piece(PieceType.KNIGHT,PieceSet.WHITE));
        Move move = new Move("C5","A4");
        KnightMoveValidator knightMoveValidator = new KnightMoveValidator();
        boolean result = knightMoveValidator.isValid(board,move);
        assertTrue(result);
    }

    @Test
    public void tryToMoveWhiteBackSHORTRightLikeaROOKClearWay(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,3,new Piece(PieceType.KNIGHT,PieceSet.WHITE));
        Move move = new Move("C5","F5");
        KnightMoveValidator knightMoveValidator = new KnightMoveValidator();
        boolean result = knightMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveOneCrossWhiteback(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,3,new Piece(PieceType.KNIGHT,PieceSet.WHITE));
        Move move = new Move("C5","B4");
        KnightMoveValidator knightMoveValidator = new KnightMoveValidator();
        boolean result = knightMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveOneCrossBlackback(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,4,new Piece(PieceType.KNIGHT,PieceSet.BLACK));
        Move move = new Move("C4","B5");
        KnightMoveValidator knightMoveValidator = new KnightMoveValidator();
        boolean result = knightMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveOneCrossWhiteForward(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,3,new Piece(PieceType.KNIGHT,PieceSet.WHITE));
        Move move = new Move("C5","B6");
        KnightMoveValidator knightMoveValidator = new KnightMoveValidator();
        boolean result = knightMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveOneCrossBlackForward(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,4,new Piece(PieceType.KNIGHT,PieceSet.BLACK));
        Move move = new Move("C4","D3");
        KnightMoveValidator knightMoveValidator = new KnightMoveValidator();
        boolean result = knightMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveNull(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,4,null);
        Move move = new Move("C4","D3");
        KnightMoveValidator knightMoveValidator = new KnightMoveValidator();
        boolean result = knightMoveValidator.isValid(board,move);
        assertFalse(result);
    }





}
