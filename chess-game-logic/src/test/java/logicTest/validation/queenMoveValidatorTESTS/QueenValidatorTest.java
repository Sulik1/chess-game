package logicTest.validation.queenMoveValidatorTESTS;

import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.logic.Move;
import com.craftincode.apps.chess.logic.validation.QueenMoveValidator;
import com.craftincode.apps.chess.piece.Piece;
import com.craftincode.apps.chess.piece.PieceSet;
import com.craftincode.apps.chess.piece.PieceType;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


import java.util.Map;
import java.util.WeakHashMap;

public class QueenValidatorTest {
    @Test
    public void stayInPlace(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,4,new Piece(PieceType.QUEEN, PieceSet.WHITE));
        Move move = new Move("E4","E4");
        QueenMoveValidator queenMoveValidator = new QueenMoveValidator();
        boolean result = queenMoveValidator.isValid(board,move);
        assertFalse(result);


    }
    @Test
    public void NoCorrectMove(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,4,new Piece(PieceType.QUEEN, PieceSet.WHITE));
        Move move = new Move("E4","H6");
        QueenMoveValidator queenMoveValidator = new QueenMoveValidator();
        boolean result = queenMoveValidator.isValid(board,move);
        assertFalse(result);


    }
    @Test
    public void tryToBeatKingAtStartOwn(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,0,new Piece(PieceType.QUEEN, PieceSet.BLACK));
        Move move = new Move("E8","D8");
        QueenMoveValidator queenMoveValidator = new QueenMoveValidator();
        boolean result = queenMoveValidator.isValid(board,move);
        assertFalse(result);


    }
}
