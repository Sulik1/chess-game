package logicTest.validation.queenMoveValidatorTESTS;

import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.logic.Move;
import com.craftincode.apps.chess.logic.validation.MoveValidator;
import com.craftincode.apps.chess.logic.validation.QueenMoveValidator;
import com.craftincode.apps.chess.piece.Piece;
import com.craftincode.apps.chess.piece.PieceSet;
import com.craftincode.apps.chess.piece.PieceType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class ParameterizedQuennMoveTestNoCorrectMoves {

    private String from,to;
    private ChessGameBoard chessGameBoard=new ChessGameBoard();

    public ParameterizedQuennMoveTestNoCorrectMoves(String from, String to) {
        this.from = from;
        this.to = to;
    }

    @Parameterized.Parameters
    public static List data(){
        return  Arrays.asList(new String[][]{{"D5","H6"},{"D5","D5"},{"D5","A1"},{"D5","B4"},{"D5","F4"},{"D5","H4"}});
    }

    @Test
    public void pawnMoveTestWithParameters_badMoves(){
        chessGameBoard.setPiece(3,3,new Piece(PieceType.QUEEN, PieceSet.WHITE));
        Move move=new Move(from,to);
        MoveValidator mv=new QueenMoveValidator();
        boolean noCorrect=mv.isValid(chessGameBoard,move);
        assertFalse(noCorrect);
    }
}
