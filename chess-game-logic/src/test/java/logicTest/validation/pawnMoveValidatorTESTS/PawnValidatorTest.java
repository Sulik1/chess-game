package logicTest.validation.pawnMoveValidatorTESTS;

import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.logic.Move;
import com.craftincode.apps.chess.logic.validation.MoveValidator;
import com.craftincode.apps.chess.logic.validation.PawnMoveValidator;
import com.craftincode.apps.chess.piece.Piece;
import com.craftincode.apps.chess.piece.PieceSet;
import com.craftincode.apps.chess.piece.PieceType;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PawnValidatorTest {
    @Test
    public void shouldPawnMoveOneForward(){
        //given
        ChessGameBoard gameBoard = new ChessGameBoard();
        gameBoard.setPiece(0, 6, new Piece(PieceType.PAWN, PieceSet.WHITE));
        Move move = new Move("A2", "A3");
        MoveValidator mv = new PawnMoveValidator();

        boolean result = mv.isValid(gameBoard, move);

        assertTrue(result);
    }

    @Test
    public void shouldPawnMoveTwoForwardIfFirstMove(){
        ChessGameBoard gameBoard = new ChessGameBoard();
        gameBoard.setPiece(0, 6, new Piece(PieceType.PAWN,PieceSet.WHITE));
        Move move = new Move("A2", "A4");
        MoveValidator mv = new PawnMoveValidator();

        boolean result = mv.isValid(gameBoard, move);

        assertTrue(result);
    }

    @Test
    public void shouldNotPawnMoveTwoForwardUnlessFirstMove(){
        ChessGameBoard gameBoard = new ChessGameBoard();
        gameBoard.setPiece(0, 5, new Piece(PieceType.PAWN,PieceSet.WHITE));
        Move move = new Move("A3", "A5");
        MoveValidator mv = new PawnMoveValidator();

        boolean result = mv.isValid(gameBoard, move);

        assertFalse(result);
    }

    @Test
    public void shouldNotWhitePawnMoveTwoForwardWhenStandingOnBlackPawnStartPosition(){
        ChessGameBoard gameBoard = new ChessGameBoard();
        gameBoard.setPiece(0, 1, new Piece(PieceType.PAWN,PieceSet.WHITE));
        Move move = new Move("A7", "A5");
        MoveValidator mv = new PawnMoveValidator();

        boolean result = mv.isValid(gameBoard, move);

        assertFalse(result);
    }

    @Test
    public void shouldBeatOpponentIfItStaysOneSquareDiagonal(){
        ChessGameBoard gameBoard = new ChessGameBoard();
        gameBoard.setPiece(0, 6, new Piece(PieceType.PAWN,PieceSet.WHITE));
        gameBoard.setPiece(1, 5, new Piece(PieceType.ROOK,PieceSet.BLACK));

        Move move = new Move("A2", "B3");
        MoveValidator mv = new PawnMoveValidator();

        boolean result = mv.isValid(gameBoard, move);

        assertTrue(result);
    }

    @Test
    public void shouldNotBeatOpponentIfItStaysOneSquareDiagonalBackward(){
        ChessGameBoard gameBoard = new ChessGameBoard();
        gameBoard.setPiece(1, 5, new Piece(PieceType.PAWN,PieceSet.WHITE));
        gameBoard.setPiece(0, 6, new Piece(PieceType.ROOK,PieceSet.BLACK));

        Move move = new Move("B3", "A2");
        MoveValidator mv = new PawnMoveValidator();

        boolean result = mv.isValid(gameBoard, move);

        assertFalse(result);
    }

    @Test
    public void shouldNotBeatOwnColor(){
        ChessGameBoard gameBoard = new ChessGameBoard();
        gameBoard.setPiece(0, 6, new Piece(PieceType.PAWN,PieceSet.WHITE));
        gameBoard.setPiece(1, 5, new Piece(PieceType.ROOK,PieceSet.WHITE));

        Move move = new Move("A2", "B3");
        MoveValidator mv = new PawnMoveValidator();

        boolean result = mv.isValid(gameBoard, move);

        assertFalse(result);
    }

    @Test
    public void shouldNotOverleapOwnColorIfMoveTwoSquares(){
        ChessGameBoard gameBoard = new ChessGameBoard();
        gameBoard.setPiece(0, 6, new Piece(PieceType.PAWN,PieceSet.WHITE));
        gameBoard.setPiece(0, 5, new Piece(PieceType.ROOK,PieceSet.WHITE));

        Move move = new Move("A2", "A4");
        MoveValidator mv = new PawnMoveValidator();

        boolean result = mv.isValid(gameBoard, move);

        assertFalse(result);
    }
    @Test
    public void canBlackPawnBeatOwnPeeceOnStart(){
        ChessGameBoard gameBoard = new ChessGameBoard();
        gameBoard.setPiece(2, 1, new Piece(PieceType.PAWN,PieceSet.BLACK));
        Move move = new Move("C7", "D8");
        MoveValidator mv = new PawnMoveValidator();

        boolean result = mv.isValid(gameBoard, move);

        assertFalse(result);
    }
    @Test
    public void canBlackPawnBeatOwnPeeceOnStart2(){
        ChessGameBoard gameBoard = new ChessGameBoard();
        gameBoard.setPiece(4, 1, new Piece(PieceType.PAWN,PieceSet.BLACK));
        Move move = new Move("E7", "D8");
        MoveValidator mv = new PawnMoveValidator();

        boolean result = mv.isValid(gameBoard, move);

        assertFalse(result);
    }
    @Test
    public void tryToMoveNull(){
        ChessGameBoard gameBoard = new ChessGameBoard();
        gameBoard.setPiece(4, 1, null);
        Move move = new Move("E7", "D8");
        MoveValidator mv = new PawnMoveValidator();

        boolean result = mv.isValid(gameBoard, move);

        assertFalse(result);
    }
}
