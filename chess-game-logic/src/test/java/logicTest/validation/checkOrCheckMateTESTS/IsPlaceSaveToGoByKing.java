package logicTest.validation.checkOrCheckMateTESTS;
import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.logic.CheckOrCheckMate;
import com.craftincode.apps.chess.logic.Move;
import com.craftincode.apps.chess.logic.validation.KingMoveValidator;
import com.craftincode.apps.chess.piece.Piece;
import com.craftincode.apps.chess.piece.PieceSet;
import com.craftincode.apps.chess.piece.PieceType;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
public class IsPlaceSaveToGoByKing {

    @Test
    public void youCantMoveThereCuzCanBeatBlack(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,0,null);
        board.setPiece(3,3,new Piece(PieceType.KING , PieceSet.BLACK));
        board.setPiece(6,4,new Piece(PieceType.ROOK,PieceSet.WHITE));
        Move move = new Move("D5","D4");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isPlaceWhereYouWantToGoIsSafe(board,move);
        assertFalse(result);

    }
    @Test
    public void youCantMoveThereCuzCanBeatWhite(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,0,null);
        board.setPiece(3,3,new Piece(PieceType.KING , PieceSet.WHITE));
        board.setPiece(6,4,new Piece(PieceType.ROOK,PieceSet.BLACK));
        Move move = new Move("D5","D4");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isPlaceWhereYouWantToGoIsSafe(board,move);
        assertFalse(result);

    }
    @Test
    public void youCanMoveThereCuzOnlyOwnCanBeatWhite(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,0,null);
        board.setPiece(3,3,new Piece(PieceType.KING , PieceSet.WHITE));
        board.setPiece(6,4,new Piece(PieceType.ROOK,PieceSet.WHITE));
        Move move = new Move("D5","D4");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isPlaceWhereYouWantToGoIsSafe(board,move);
        assertTrue(result);

    }
    @Test
    public void youCanMoveThereCuzOnlyOwnCanBeatBlack(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,0,null);
        board.setPiece(3,3,new Piece(PieceType.KING , PieceSet.BLACK));
        board.setPiece(6,4,new Piece(PieceType.ROOK,PieceSet.BLACK));
        Move move = new Move("D5","D4");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isPlaceWhereYouWantToGoIsSafe(board,move);
        assertTrue(result);

    }
    @Test
    public void onlycantonleft(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(0,3,new Piece(PieceType.KING , PieceSet.WHITE));
        Move move = new Move("A5","A4");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isPlaceWhereYouWantToGoIsSafe(board,move);
        assertTrue(result);

    }
    @Test
    public void noSpaceEpmtyAroundOpponentBeat2(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(7,3,new Piece(PieceType.ROOK, PieceSet.BLACK));
        board.setPiece(7,4,new Piece(PieceType.ROOK, PieceSet.BLACK));
        board.setPiece(0,4,new Piece(PieceType.ROOK, PieceSet.BLACK));
        Move move = new Move("D4","D3");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isPlaceWhereYouWantToGoIsSafe(board,move);
        assertTrue(result);
    }
    @Test
    public void OnePossisNoEmpty2(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(2,4,new Piece(PieceType.KING,PieceSet.WHITE));
        board.setPiece(0,3,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        Move move = new Move("C4","C5");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isPlaceWhereYouWantToGoIsSafe(board,move);
        assertTrue(result);

    }
}
