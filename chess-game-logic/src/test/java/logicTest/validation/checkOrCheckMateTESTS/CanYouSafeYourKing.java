package logicTest.validation.checkOrCheckMateTESTS;
import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.logic.CheckOrCheckMate;
import com.craftincode.apps.chess.piece.Piece;
import com.craftincode.apps.chess.piece.PieceSet;
import com.craftincode.apps.chess.piece.PieceType;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
public class CanYouSafeYourKing {

    @Test
    public void youCanHelpYourKing(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(6, 4, new Piece(PieceType.ROOK, PieceSet.BLACK));
        board.setPiece(7, 5, new Piece(PieceType.PAWN, PieceSet.WHITE));


        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.canYouSafeYourKingByBeatingSomeoneWhoCanBeatKing(board,true);
        assertTrue(result);
    }
    @Test
    public void youCantHelpYourKing(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(6, 4, new Piece(PieceType.ROOK, PieceSet.BLACK));

        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.canYouSafeYourKingByBeatingSomeoneWhoCanBeatKing(board,true);
        assertFalse(result);
    }
    @Test
    public void youCanHelpYourKing1(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(7, 4, new Piece(PieceType.ROOK, PieceSet.BLACK));
        board.setPiece(7, 6, new Piece(PieceType.ROOK, PieceSet.WHITE));

        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.canYouSafeYourKingByBeatingSomeoneWhoCanBeatKing(board,true);
        assertTrue(result);
    }

    @Test
    public void youCantHelpYourKingCuz2ofThemCanBeatHim(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(6, 4, new Piece(PieceType.ROOK, PieceSet.BLACK));
        board.setPiece(0, 4, new Piece(PieceType.ROOK, PieceSet.BLACK));

        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.canYouSafeYourKingByBeatingSomeoneWhoCanBeatKing(board,true);
        assertFalse(result);
    }
    @Test
    public void youCantHelpYourKingCuz2ofThemCanBeatHimBLACK(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(0,4,null);
        board.setPiece(3, 3, new Piece(PieceType.KING, PieceSet.BLACK));
        board.setPiece(6, 3, new Piece(PieceType.ROOK, PieceSet.WHITE));
        board.setPiece(0, 3, new Piece(PieceType.ROOK, PieceSet.WHITE));

        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.canYouSafeYourKingByBeatingSomeoneWhoCanBeatKing(board,false);
        assertFalse(result);
    }
    @Test
    public void youCantCuzOfCheckmate(){

        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(6,2,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        board.setPiece(7,2,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        board.setPiece(0,3,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        board.setPiece(0,2,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        board.setPiece(7,3,new Piece(PieceType.ROOK, PieceSet.BLACK));

        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.canYouSafeYourKingByBeatingSomeoneWhoCanBeatKing(board,true);
        assertFalse(result);
    }
}
