package logicTest.validation.checkOrCheckMateTESTS;

import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.logic.CheckOrCheckMate;
import com.craftincode.apps.chess.piece.Piece;
import com.craftincode.apps.chess.piece.PieceSet;
import com.craftincode.apps.chess.piece.PieceType;
import org.junit.Test;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CanSomeOneBeatKing {

    @Test
    public void canSomeOneBeatWhiteKing() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(6, 4, new Piece(PieceType.ROOK, PieceSet.BLACK));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.isCheck(board);
        assertTrue(result);

    }

    @Test
    public void canSomeOneBeatWhiteKing1() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(4, 3, new Piece(PieceType.PAWN, PieceSet.BLACK));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.isCheck(board);
        assertTrue(result);

    }

    @Test
    public void canSomeOneBeatWhiteKing2() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(5, 2, new Piece(PieceType.BISHOP, PieceSet.BLACK));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.isCheck(board);
        assertTrue(result);

    }

    @Test
    public void cantSomeOneBeatWhiteKing() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(6, 5, new Piece(PieceType.ROOK, PieceSet.BLACK));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.isCheck(board);
        assertFalse(result);

    }

    @Test
    public void cantSomeOneBeatWhiteKing1() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(5, 3, new Piece(PieceType.PAWN, PieceSet.BLACK));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.isCheck(board);
        assertFalse(result);

    }

    @Test
    public void cantSomeOneBeatWhiteKing2() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(5, 2, new Piece(PieceType.BISHOP, PieceSet.WHITE));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.isCheck(board);
        assertFalse(result);

    }

    @Test
    public void canSomeOneBeatBlackKing() {
        ChessGameBoard board = new ChessGameBoard();

        board.setPiece(4,0,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.BLACK));
        board.setPiece(6, 4, new Piece(PieceType.ROOK, PieceSet.WHITE));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.isCheck(board);
        assertTrue(result);

    }

    @Test
    public void canSomeOneBeatBlackKing1() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,0,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.BLACK));
        board.setPiece(4, 5, new Piece(PieceType.PAWN, PieceSet.WHITE));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.isCheck(board);
        assertTrue(result);

    }

    @Test
    public void canSomeOneBeatBlackKing2() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,0,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.BLACK));
        board.setPiece(5, 2, new Piece(PieceType.BISHOP, PieceSet.WHITE));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.isCheck(board);
        assertTrue(result);

    }

    @Test
    public void cantSomeOneBeatBlackKing() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,0,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.BLACK));
        board.setPiece(6, 5, new Piece(PieceType.ROOK, PieceSet.WHITE));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.isCheck(board);
        assertFalse(result);

    }

    @Test
    public void cantSomeOneBeatBlackKing1() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,0,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.BLACK));
        board.setPiece(5, 3, new Piece(PieceType.PAWN, PieceSet.WHITE));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.isCheck(board);
        assertFalse(result);

    }

    @Test
    public void cantSomeOneBeatBlackKing2() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,0,null);
        board.setPiece(4, 3, new Piece(PieceType.KING, PieceSet.BLACK));
        board.setPiece(5, 2, new Piece(PieceType.BISHOP, PieceSet.BLACK));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.isCheck(board);
        assertFalse(result);
    }
    @Test
    public void cantSomeOneBeatBlackKing3PAwn() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,0,null);
        board.setPiece(4, 3, new Piece(PieceType.KING, PieceSet.BLACK));
        board.setPiece(4, 4, new Piece(PieceType.PAWN, PieceSet.WHITE));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.isCheck(board);
        assertFalse(result);
    }
    @Test
    public void cantSomeOneBeatBlackKing4PAwn() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,0,null);
        board.setPiece(4, 4, new Piece(PieceType.KING, PieceSet.BLACK));
        board.setPiece(4, 6, new Piece(PieceType.PAWN, PieceSet.WHITE));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.isCheck(board);
        assertFalse(result);
    }
}
