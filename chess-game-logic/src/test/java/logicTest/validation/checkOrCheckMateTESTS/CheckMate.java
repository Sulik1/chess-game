package logicTest.validation.checkOrCheckMateTESTS;
import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.logic.CheckOrCheckMate;
import com.craftincode.apps.chess.piece.Piece;
import com.craftincode.apps.chess.piece.PieceSet;
import com.craftincode.apps.chess.piece.PieceType;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
public class CheckMate {

    @Test
    public void isCheckMateWhite(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(6,2,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        board.setPiece(7,2,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        board.setPiece(0,3,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        board.setPiece(0,2,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        board.setPiece(7,3,new Piece(PieceType.ROOK, PieceSet.BLACK));
        board.setPiece(3,2,new Piece(PieceType.ROOK, PieceSet.BLACK));

        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.isCheckMate(board,true);
        assertTrue(result);
    }
    @Test
    public void isCheckMateBlack(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,0,null);

        board.setPiece(4, 4, new Piece(PieceType.KING, PieceSet.BLACK));
        board.setPiece(7,3,new Piece(PieceType.ROOK, PieceSet.WHITE));
        board.setPiece(7,4,new Piece(PieceType.ROOK, PieceSet.WHITE));
        board.setPiece(7,5,new Piece(PieceType.ROOK, PieceSet.WHITE));

        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.isCheckMate(board,false);
        assertTrue(result);


    }
    @Test
    public void noCheckMateBlack(){

    }
    @Test
    public void noCheckMateWhite(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(7,2,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        board.setPiece(0,3,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        board.setPiece(0,2,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        board.setPiece(7,3,new Piece(PieceType.ROOK, PieceSet.BLACK));
        board.setPiece(7,5,new Piece(PieceType.ROOK, PieceSet.WHITE));

        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.isCheckMate(board,true);
        assertFalse(result);
    }
}
