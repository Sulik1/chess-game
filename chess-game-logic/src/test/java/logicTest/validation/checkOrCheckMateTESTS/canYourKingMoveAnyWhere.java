package logicTest.validation.checkOrCheckMateTESTS;

import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.logic.CheckOrCheckMate;
import com.craftincode.apps.chess.piece.Piece;
import com.craftincode.apps.chess.piece.PieceSet;
import com.craftincode.apps.chess.piece.PieceType;
import javafx.beans.binding.When;
import org.junit.Test;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class canYourKingMoveAnyWhere {

    @Test
    public void emptySpaceToMoveAround(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.canYourKingMoveAnyWhere(board,true);
        assertTrue(result);
    }
    @Test
    public void noSpaceOnBackButOtherEmpty(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 5, new Piece(PieceType.KING, PieceSet.WHITE));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.canYourKingMoveAnyWhere(board,true);
        assertTrue(result);
    }
    @Test
    public void noSpaceEmptyAroundFriends(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 5, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(2,5,new Piece(PieceType.PAWN, PieceSet.WHITE));
        board.setPiece(4,5,new Piece(PieceType.PAWN, PieceSet.WHITE));
        board.setPiece(2,4,new Piece(PieceType.PAWN, PieceSet.WHITE));
        board.setPiece(3,4,new Piece(PieceType.PAWN, PieceSet.WHITE));
        board.setPiece(4,4,new Piece(PieceType.PAWN, PieceSet.WHITE));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.canYourKingMoveAnyWhere(board,true);
        assertFalse(result);
    }
    @Test
    public void noSpaceEpmtyAroundOpponentBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(6,2,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.canYourKingMoveAnyWhere(board,true);
        assertTrue(result);
    }
    @Test
    public void noSpaceEpmtyAroundOpponentBeat2(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(7,3,new Piece(PieceType.ROOK, PieceSet.BLACK));
         board.setPiece(7,4,new Piece(PieceType.ROOK, PieceSet.BLACK));
         board.setPiece(0,4,new Piece(PieceType.ROOK, PieceSet.BLACK));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.canYourKingMoveAnyWhere(board,true);
        assertTrue(result);
    }

    @Test
    public void noSpaceOnLeftSideCuzOfEndOfBoard(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(0, 3, new Piece(PieceType.KING, PieceSet.WHITE));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.canYourKingMoveAnyWhere(board,true);
        assertTrue(result);



    }
    @Test
    public void noSpaceOnLeftSideCuzOfEndOfBoardAndNoSpaceTomoveonRight(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);

        board.setPiece(0, 3, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(0, 4, new Piece(PieceType.PAWN, PieceSet.WHITE));
        board.setPiece(1, 4, new Piece(PieceType.PAWN, PieceSet.WHITE));
        board.setPiece(6, 3, new Piece(PieceType.ROOK, PieceSet.BLACK));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();


        boolean result = checkOrCheckMate.canYourKingMoveAnyWhere(board,true);
        assertFalse(result);



    }
    @Test
    public void OnePossisNoEmpty(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(2,4,new Piece(PieceType.KING,PieceSet.WHITE));
        board.setPiece(6,3,new Piece(PieceType.ROOK,PieceSet.BLACK));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.canYourKingMoveAnyWhere(board,true);
        assertTrue(result);

    }
    @Test
    public void OnePossisNoEmpty2(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,0,null);
        board.setPiece(2,4,new Piece(PieceType.KING,PieceSet.BLACK));
        board.setPiece(0,3,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.canYourKingMoveAnyWhere(board,false);
        assertTrue(result);

    }
    @Test
    public void twoPossisNoEmpty3(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,0,null);
        board.setPiece(2,4,new Piece(PieceType.KING,PieceSet.BLACK));
        board.setPiece(4,3,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.canYourKingMoveAnyWhere(board,false);
        assertTrue(result);

    }
    @Test
    public void correct(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3,4,new Piece(PieceType.KING,PieceSet.WHITE));
        board.setPiece(0,2,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.canYourKingMoveAnyWhere(board,true);
        assertTrue(result);

    }
    @Test
    public void cantMoveCuzOfCheckMAte(){

        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(6,2,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        board.setPiece(7,2,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        board.setPiece(0,3,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        board.setPiece(0,2,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        board.setPiece(7,3,new Piece(PieceType.ROOK, PieceSet.BLACK));

        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        boolean result = checkOrCheckMate.canYourKingMoveAnyWhere(board,true);
        assertFalse(result);
    }





}
