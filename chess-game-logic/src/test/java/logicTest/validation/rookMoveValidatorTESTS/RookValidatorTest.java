package logicTest.validation.rookMoveValidatorTESTS;

import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.logic.CheckOrCheckMate;
import com.craftincode.apps.chess.logic.Move;
import com.craftincode.apps.chess.logic.validation.BishopMoveValidator;
import com.craftincode.apps.chess.logic.validation.MoveValidator;
import com.craftincode.apps.chess.logic.validation.RookMoveValidator;
import com.craftincode.apps.chess.piece.Piece;
import com.craftincode.apps.chess.piece.PieceSet;
import com.craftincode.apps.chess.piece.PieceType;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RookValidatorTest {
    @Test
    public void shouldNotMoveNullObject(){
        ChessGameBoard board = new ChessGameBoard();
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        Move move = new Move("A4","A7");
        boolean result = rookMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void shouldNotStayInPlaceWhite(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(0,3,new Piece(PieceType.ROOK,PieceSet.WHITE));
        Move move = new Move("A4","A4");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board,move);

        assertFalse(result);
    }
    @Test
    public void shouldNotStayInPlaceBlack(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(0,3,new Piece(PieceType.ROOK,PieceSet.BLACK));
        Move move = new Move("A4","A4");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board,move);

        assertFalse(result);
    }
    @Test
    public void tryToMoveForwardWHITEWithSomePieceOnHisWaySHOULDFALSE(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(0,5,new Piece(PieceType.ROOK,PieceSet.WHITE));
        board.setPiece(0,4, new Piece(PieceType.PAWN,PieceSet.WHITE));
        Move move = new Move("A3","A6");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();

        boolean result = rookMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveForwadWHITEclearWayAndNotBEAT(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(0,5,new Piece(PieceType.ROOK,PieceSet.WHITE));
        Move move = new Move("A3","A6");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();

        boolean result = rookMoveValidator.isValid(board,move);

        assertTrue(result);

    }
    @Test
    public void tryToMoveForwadWhiteClearWayAndBEAT(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(0,5,new Piece(PieceType.ROOK,PieceSet.WHITE));
        board.setPiece(0,2,new Piece(PieceType.PAWN,PieceSet.BLACK));
        Move move = new Move("A3","A6");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();

        boolean result = rookMoveValidator.isValid(board,move);

        assertTrue(result);

    }
     @Test
    public void tryToMoveBackWhiteClearWayNoBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(0,2,new Piece(PieceType.ROOK,PieceSet.WHITE));
        Move move = new Move("A6","A3");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();

        boolean result = rookMoveValidator.isValid(board,move);

        assertTrue(result);
    }
    @Test
    public void tryToMoveBackWhiteNOTClearWayNoBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(0,2,new Piece(PieceType.ROOK,PieceSet.WHITE));
        board.setPiece(0,3,new Piece(PieceType.PAWN,PieceSet.WHITE));
        Move move = new Move("A6","A3");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();

        boolean result = rookMoveValidator.isValid(board,move);

        assertFalse(result);
    }
    @Test
    public void tryToMoveBackWhiteClearWayAndBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(0,2,new Piece(PieceType.ROOK,PieceSet.WHITE));
        board.setPiece(0,5,new Piece(PieceType.ROOK,PieceSet.BLACK));
        Move move = new Move("A6","A3");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();

        boolean result = rookMoveValidator.isValid(board,move);

        assertTrue(result);
    }
    @Test
    public void tryToMoveWhiteRightClearWayNotBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(0,3,new Piece(PieceType.ROOK,PieceSet.WHITE));
        Move move = new Move("A5","F5");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board,move);

        assertTrue(result);
    }
    @Test
    public void tryToMoveWhiteRightNotClearWayNotBeatShouldFalse(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(0,3,new Piece(PieceType.ROOK,PieceSet.WHITE));
        board.setPiece(2,3,new Piece(PieceType.PAWN,PieceSet.WHITE));
        Move move = new Move("A5","F5");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board,move);

        assertFalse(result);
    }
    @Test
    public void tryToMoveWhiteRightClearWayAndBeatOwnShouldFalse(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(0,3,new Piece(PieceType.ROOK,PieceSet.WHITE));
        board.setPiece(5,3,new Piece(PieceType.PAWN,PieceSet.WHITE));
        Move move = new Move("A5","F5");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board,move);

        assertFalse(result);
    }
    @Test
    public void tryToMoveWhiteRightClearWayAndBeatOpponent(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(0,3,new Piece(PieceType.ROOK,PieceSet.WHITE));
        board.setPiece(5,3,new Piece(PieceType.PAWN,PieceSet.BLACK));
        Move move = new Move("A5","F5");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board,move);

        assertTrue(result);
    }
    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void tryToMoveWhiteRightOutOfBoardShouldFalse(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(0,3,new Piece(PieceType.ROOK,PieceSet.WHITE));
        Move move = new Move("A5","I5");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board,move);
        assertFalse(result);

    }
    @Test
    public void tryToMoveRightWhiteNotInOYShouldFalse(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(0,3,new Piece(PieceType.ROOK,PieceSet.WHITE));
        Move move = new Move("A5","C4");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveWhiteLeftClearWayNoBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,3,new Piece(PieceType.ROOK,PieceSet.WHITE));
        Move move = new Move("E5","A5");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveWhiteLeftNotClearWayShouldFalse(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,3,new Piece(PieceType.ROOK,PieceSet.WHITE));
        board.setPiece(2,3,new Piece(PieceType.PAWN,PieceSet.BLACK));
        Move move = new Move("E5","A5");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveWhiteLeftClearWayAndBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,3,new Piece(PieceType.ROOK,PieceSet.WHITE));
        board.setPiece(0,3,new Piece(PieceType.PAWN,PieceSet.BLACK));
        Move move = new Move("E5","A5");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveLeftWhiteNotInOYShouldFalse(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,3,new Piece(PieceType.ROOK,PieceSet.WHITE));
        Move move = new Move("E5","C4");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveBlackForwardClearWayNoBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(0,3,new Piece(PieceType.ROOK,PieceSet.BLACK));
        Move move = new Move("A5","A3");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board,move);

        assertTrue(result);
    }
    @Test
    public void tryToMoveBlackForwardNoClearWay() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(1, 2, new Piece(PieceType.ROOK, PieceSet.BLACK));
        board.setPiece(1, 4, new Piece(PieceType.PAWN, PieceSet.BLACK));
        Move move = new Move("B6", "B3");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board, move);

        assertFalse(result);
    }
    @Test
    public void tryToMoveBlackForwardAndBeat() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(1, 2, new Piece(PieceType.ROOK, PieceSet.BLACK));
        board.setPiece(1, 4, new Piece(PieceType.PAWN, PieceSet.WHITE));
        Move move = new Move("B6", "B4");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board, move);

        assertTrue(result);
    }
    @Test
    public void tryToMoveBlackCross() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(1, 2, new Piece(PieceType.ROOK, PieceSet.BLACK));
        Move move = new Move("B6", "C4");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board, move);

        assertFalse(result);
    }
    @Test
    public void tryToMoveBlackBackClearWayNoBeat() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(1, 4, new Piece(PieceType.ROOK, PieceSet.BLACK));
        Move move = new Move("B4", "B6");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board, move);

        assertTrue(result);
    }
    @Test
    public void tryToMoveBlackBackNoClearWay() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(1, 4, new Piece(PieceType.ROOK, PieceSet.BLACK));
        board.setPiece(1, 3, new Piece(PieceType.ROOK, PieceSet.WHITE));
        Move move = new Move("B4", "B6");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board, move);

        assertFalse(result);
    }
    @Test
    public void tryToMoveBlackBackClearWayAndBeat() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(1, 4, new Piece(PieceType.ROOK, PieceSet.BLACK));
        board.setPiece(1, 2, new Piece(PieceType.ROOK, PieceSet.WHITE));
        Move move = new Move("B4", "B6");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board, move);

        assertTrue(result);
    }
    @Test
    public void tryToMoveBlackRightClearWay() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(1, 4, new Piece(PieceType.ROOK, PieceSet.BLACK));
        Move move = new Move("B4", "E4");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board, move);

        assertTrue(result);
    }
    @Test
    public void tryToMoveBlackRightNoClearWay() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(1, 4, new Piece(PieceType.ROOK, PieceSet.BLACK));
        board.setPiece(3, 4, new Piece(PieceType.ROOK, PieceSet.BLACK));
        Move move = new Move("B4", "F4");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board, move);

        assertFalse(result);
    }
    @Test
    public void tryToMoveBlackRightAndBeat() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(1, 4, new Piece(PieceType.ROOK, PieceSet.BLACK));
        board.setPiece(5, 4, new Piece(PieceType.ROOK, PieceSet.WHITE));
        Move move = new Move("B4", "F4");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board, move);

        assertTrue(result);
    }
    @Test
    public void tryToMoveBlackLeftAndBeat() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(5, 4, new Piece(PieceType.ROOK, PieceSet.BLACK));
        board.setPiece(1, 4, new Piece(PieceType.ROOK, PieceSet.WHITE));
        Move move = new Move("F4", "B4");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board, move);

        assertTrue(result);
    }
    @Test
    public void tryToMoveBlackLeftNoClearWay() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(5, 4, new Piece(PieceType.ROOK, PieceSet.BLACK));
        board.setPiece(3, 4, new Piece(PieceType.ROOK, PieceSet.WHITE));
        Move move = new Move("F4", "B4");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board, move);

        assertFalse(result);
    }
    @Test
    public void tryToMoveBlackLeftClearWay() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(5, 4, new Piece(PieceType.ROOK, PieceSet.BLACK));
        Move move = new Move("F4", "B4");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board, move);

        assertTrue(result);
    }
    @Test
    public void NoCorrectMove(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,4,new Piece(PieceType.QUEEN, PieceSet.WHITE));
        Move move = new Move("E4","H6");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board, move);
        assertFalse(result);


    }
    @Test
    public void canSomeOneBeatWhiteKing() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,7,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(6, 4, new Piece(PieceType.ROOK, PieceSet.BLACK));
        Move move = new Move("G4","D4");
        RookMoveValidator rookMoveValidator = new RookMoveValidator();
        boolean result = rookMoveValidator.isValid(board,move);
        assertTrue(result);

    }





}
