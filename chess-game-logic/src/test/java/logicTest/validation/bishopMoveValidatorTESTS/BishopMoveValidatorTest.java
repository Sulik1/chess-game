package logicTest.validation.bishopMoveValidatorTESTS;
import com.craftincode.apps.chess.logic.validation.BishopMoveValidator;
import com.craftincode.apps.chess.logic.validation.KingMoveValidator;
import com.craftincode.apps.chess.logic.validation.KnightMoveValidator;
import com.craftincode.apps.chess.logic.validation.QueenMoveValidator;
import org.junit.Test;
import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.logic.Move;
import com.craftincode.apps.chess.piece.Piece;
import com.craftincode.apps.chess.piece.PieceSet;
import com.craftincode.apps.chess.piece.PieceType;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
public class BishopMoveValidatorTest {

    @Test
    public void tryToMoveForwardRightWhiteClearWayNoBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,5,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        Move move = new Move("D3","F5");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveForwardLeftWhiteClearWayNoBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,5,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        Move move = new Move("D3","B5");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveForwardLeftWhiteClearWayBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,5,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        board.setPiece(1,3,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        Move move = new Move("D3","B5");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveForwardRightWhiteClearWayBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,5,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        board.setPiece(5,3,new Piece(PieceType.PAWN,PieceSet.BLACK));
        Move move = new Move("D3","F5");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveForwardRightWhiteNoClearWay(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,5,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        board.setPiece(4,4,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        Move move = new Move("D3","F5");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveForwardLeftWhiteNoClearWay (){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,5,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        board.setPiece(1,3,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        Move move = new Move("D3","A6");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveForwardLeftWhiteAndBeatOwn(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,5,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        board.setPiece(1,3,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        Move move = new Move("D3","B5");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveBlackLeftBackClearWayNoBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(6,5,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        Move move = new Move("G3","D6");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertTrue(result);

    }
    @Test
    public void tryToMoveBlackLeftBackNOClearWay(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(6,5,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        board.setPiece(5,4,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        Move move = new Move("G3","D6");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);

    }
    @Test
    public void tryToMoveBlackLeftBackClearWayBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(6,5,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        board.setPiece(3,2,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        Move move = new Move("G3","D6");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertTrue(result);

    }
    @Test
    public void tryToMoveWhiteRightForwardClearWayNoBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,5,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        Move move = new Move("D3","G6");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertTrue(result);

    }
    @Test
    public void tryToMoveWhiteRightForwardNoClearWay(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,5,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        board.setPiece(4,4,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        Move move = new Move("D3","G6");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);

    }
    @Test
    public void tryToMoveWhiteRightForwardClearWayBeatOwn(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,5,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        board.setPiece(6,2,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        Move move = new Move("D3","G6");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);

    }
    @Test
    public void tryToMoveWhiteRightForwardClearWayBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,5,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        board.setPiece(6,2,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        Move move = new Move("D3","G6");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertTrue(result);

    }
    @Test
    public void tryToMoveBlackRightBackClearWay(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(1,5,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        Move move = new Move("B3","E6");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertTrue(result);

    }
    @Test
    public void tryToMoveBlackRightBackNoClearWay(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(1,5,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        board.setPiece(3,3,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        Move move = new Move("B3","E6");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);

    }
    @Test
    public void tryToMoveBlackRightBackClearWayBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(1,5,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        board.setPiece(4,2,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        Move move = new Move("B3","E6");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertTrue(result);

    }
    @Test
    public void tryToMoveWhiteBackLeftClearWayNoBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(6,2,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        Move move = new Move("G6","D3");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertTrue(result);


    }
    @Test
    public void tryToMoveBlackForwardLeftClearWayNoBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(6,2,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        Move move = new Move("G6","D3");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertTrue(result);


    }
    @Test
    public void tryToMoveWhiteBackLeftClearWayBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(6,2,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        board.setPiece(3,5,new Piece(PieceType.PAWN,PieceSet.BLACK));
        Move move = new Move("G6","D3");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertTrue(result);


    }
    @Test
    public void tryToMoveBlackForwardLeftClearWayBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(6,2,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        board.setPiece(3,5,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        Move move = new Move("G6","D3");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertTrue(result);


    }
    @Test
    public void tryToMoveWhiteBackLeftNoClearWay(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(6,2,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        board.setPiece(4,4,new Piece(PieceType.PAWN,PieceSet.BLACK));
        Move move = new Move("G6","D3");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);


    }
    @Test
    public void tryToMoveBlackForwardLeftNoClearWay(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(6,2,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        board.setPiece(4,4,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        Move move = new Move("G6","D3");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);


    }
    @Test
    public void tryToMoveWhiteBackRightClearWayNoBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,2,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        Move move = new Move("C6","F3");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertTrue(result);


    }
    @Test
    public void tryToMoveBlackForwardRightClearWayNoBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,2,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        Move move = new Move("C6","F3");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertTrue(result);


    }

    @Test
    public void tryToMoveWhiteBackRightClearWayBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,2,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        board.setPiece(5,5,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        Move move = new Move("C6","F3");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertTrue(result);


    }
    @Test
    public void tryToMoveBlackForwardRightClearWayBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,2,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        board.setPiece(5,5,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        Move move = new Move("C6","F3");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertTrue(result);


    }
    @Test
    public void tryToMoveWhiteBackRightNoClearWay(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,2,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        board.setPiece(4,4,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        Move move = new Move("C6","F3");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);


    }
    @Test
    public void tryToMoveBlackForwardRightNoClearWayOnFirstStep(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,2,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        board.setPiece(3,3,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        Move move = new Move("C6","F3");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);


    }
    @Test
    public void tryToMoveWhiteBackRightNoClearWayOnFirstStep(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,2,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        board.setPiece(3,3,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        Move move = new Move("C6","F3");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);


    }
    @Test
    public void tryToMoveBlackForwardRightNoClearWay(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,2,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        board.setPiece(4,4,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        Move move = new Move("C6","F3");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);


    }
    @Test
    public void NoCorrectMove(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,4,new Piece(PieceType.BISHOP, PieceSet.WHITE));
        Move move = new Move("E4","H6");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);


    }
    @Test
    public void NoCorrectMove1(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,4,new Piece(PieceType.BISHOP, PieceSet.WHITE));
        Move move = new Move("E4","F6");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);


    }
    @Test
    public void NoCorrectMove2(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,4,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        Move move = new Move("E4","D6");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);


    }
    @Test
    public void NoCorrectMove4(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(6,2,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        Move move = new Move("G6","C3");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);


    }
    @Test
    public void NoCorrectMove5(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(6,2,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        Move move = new Move("G6","E3");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);


    }
    @Test
    public void NoCorrectMove6(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(6,2,new Piece(PieceType.BISHOP, PieceSet.WHITE));
        Move move = new Move("G6","A3");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);


    }
    @Test
    public void NoCorrectMove7(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(6,2,new Piece(PieceType.BISHOP, PieceSet.WHITE));
        Move move = new Move("G6","F3");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);


    }
    @Test
    public void NoCorrectMove8(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,2,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        Move move = new Move("C6","E3");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);


    }
    @Test
    public void NoCorrectMove9(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,2,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        Move move = new Move("C6","G3");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);


    }
    @Test
    public void tryToMoveForwardRightWhiteClearWayNoBeatAtStart(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,5,new Piece(PieceType.BISHOP,PieceSet.WHITE));
        Move move = new Move("F1","G2");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToBeatKingOwnAtStart(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,0,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        Move move = new Move("C8","D8");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test()
    public void tryToMoveNullObject(){
        ChessGameBoard board = new ChessGameBoard();
        Move move = new Move("E5","D4");
        BishopMoveValidator bishopMoveValidator = new BishopMoveValidator();
        boolean result = bishopMoveValidator.isValid(board,move);
        assertFalse(result);
    }












}
