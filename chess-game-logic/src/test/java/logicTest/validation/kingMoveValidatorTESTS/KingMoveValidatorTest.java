package logicTest.validation.kingMoveValidatorTESTS;

import com.craftincode.apps.chess.board.ChessGameBoard;
import com.craftincode.apps.chess.logic.CheckOrCheckMate;
import com.craftincode.apps.chess.logic.Move;
import com.craftincode.apps.chess.logic.MoveValidationFabric;
import com.craftincode.apps.chess.logic.validation.KingMoveValidator;
import com.craftincode.apps.chess.piece.Piece;
import com.craftincode.apps.chess.piece.PieceSet;
import com.craftincode.apps.chess.piece.PieceType;
import org.junit.Test;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class KingMoveValidatorTest {

    @Test
    public void tryToMoveForward(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,4,new Piece(PieceType.KING, PieceSet.WHITE));
        Move move = new Move("D4","D5");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveToMuchForward(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,4,new Piece(PieceType.KING, PieceSet.WHITE));
        Move move = new Move("D4","D6");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveBack(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,3,new Piece(PieceType.KING, PieceSet.BLACK));
        Move move = new Move("D5","D4");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveToMuchBack(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,3,new Piece(PieceType.KING, PieceSet.WHITE));
        Move move = new Move("D5","D3");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveLeft(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,3,new Piece(PieceType.KING, PieceSet.WHITE));
        Move move = new Move("D5","C5");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveToMuchLeft(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,3,new Piece(PieceType.KING, PieceSet.BLACK));
        Move move = new Move("D5","B5");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveRight(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,3,new Piece(PieceType.KING, PieceSet.WHITE));
        Move move = new Move("D5","E5");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveToMuchRight(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,3,new Piece(PieceType.KING, PieceSet.WHITE));
        Move move = new Move("D5","F5");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveCrossLeftUp(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,4,new Piece(PieceType.KING, PieceSet.WHITE));
        Move move = new Move("D4","C5");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveToMuchCrossLeftUp(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,4,new Piece(PieceType.KING, PieceSet.WHITE));
        Move move = new Move("D4","B6");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveCrossRightUp(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,3,new Piece(PieceType.KING, PieceSet.BLACK));
        Move move = new Move("D5","E6");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveToMuchCrossRightUp(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,4,new Piece(PieceType.KING, PieceSet.WHITE));
        Move move = new Move("D4","F6");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveCrossRightDown(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,3,new Piece(PieceType.KING, PieceSet.BLACK));
        Move move = new Move("D5","E4");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveToMuchCrossRightDown(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,3,new Piece(PieceType.KING, PieceSet.WHITE));
        Move move = new Move("D5","F3");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveCrossLeftDown(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,3,new Piece(PieceType.KING, PieceSet.BLACK));
        Move move = new Move("D5","C4");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveToMuchCrossLeftDown(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,3,new Piece(PieceType.KING, PieceSet.WHITE));
        Move move = new Move("D5","B3");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToStayInPlace(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,3,new Piece(PieceType.KING, PieceSet.WHITE));
        Move move = new Move("D5","D5");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToBeatOwnWhite(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,3,new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(3,4,new Piece(PieceType.PAWN, PieceSet.WHITE));
        Move move = new Move("D5","D4");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToBeatOwnBlack(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,3,new Piece(PieceType.KING, PieceSet.BLACK));
        board.setPiece(4,3,new Piece(PieceType.PAWN, PieceSet.BLACK));
        Move move = new Move("D5","E5");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToBeatOponentWhite(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,3,new Piece(PieceType.KING, PieceSet.BLACK));
        board.setPiece(3,4,new Piece(PieceType.PAWN, PieceSet.WHITE));
        Move move = new Move("D5","D4");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToBeatOponentBlack(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,3,new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(4,3,new Piece(PieceType.PAWN, PieceSet.BLACK));
        Move move = new Move("D5","E5");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveRightForward(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(3,7,new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(2,6,null);
        Move move = new Move("D1","C2");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryToMoveToPlaceWhereSomeOneCanBeatYouWhite(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,4,new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(6,3,new Piece(PieceType.ROOK, PieceSet.BLACK));
        Move move = new Move("C4","C5");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertFalse(result);
    }@Test
    public void tryToMoveToPlaceWhereSomeOneCanBeatYouBlack(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2,4,new Piece(PieceType.KING, PieceSet.BLACK));
        board.setPiece(6,3,new Piece(PieceType.ROOK, PieceSet.WHITE));
        Move move = new Move("C4","C5");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveToPlaceWhereSomeOneCanBeatYouWhite1() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(5, 2, new Piece(PieceType.BISHOP, PieceSet.BLACK));
        Move move = new Move("C4", "D4");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board, move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveToPlaceWhereSomeOneCanBeatYouBlack1() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2, 4, new Piece(PieceType.KING, PieceSet.BLACK));
        board.setPiece(5, 2, new Piece(PieceType.BISHOP, PieceSet.WHITE));
        Move move = new Move("C4", "D4");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board, move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveToInPlace() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(2, 4, new Piece(PieceType.KING, PieceSet.BLACK));
        Move move = new Move("C4", "C4");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board, move);
        assertFalse(result);
    }
    @Test
    public void noSpaceEpmtyAroundOpponentBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(6,2,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        board.setPiece(7,2,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        board.setPiece(0,3,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        board.setPiece(0,2,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        //board.setPiece(7,3,new Piece(PieceType.ROOK, PieceSet.BLACK));

        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        KingMoveValidator kingMoveValidator =new KingMoveValidator();
        Move move = new Move("D4","D5");
        boolean result = kingMoveValidator.isValid(board,move);
        assertTrue(result);

    }
    @Test
    public void BishopBeat(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(6,2,new Piece(PieceType.BISHOP, PieceSet.BLACK));
        CheckOrCheckMate checkOrCheckMate = new CheckOrCheckMate();
        KingMoveValidator kingMoveValidator =new KingMoveValidator();
        Move move = new Move("D4","E4");
        boolean result = kingMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void tryToMoveNullObj()
    {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(1,3,null);
        Move move = new Move("B5","B6");
        KingMoveValidator kingMoveValidator =new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertFalse(result);

    }
    @Test
    public void tryToMoveFromBeatenPosToSafe(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(7,4,new Piece(PieceType.ROOK, PieceSet.BLACK));
        KingMoveValidator kingMoveValidator =new KingMoveValidator();
        Move move = new Move("D4","D5");
        boolean result = kingMoveValidator.isValid(board,move);
        assertTrue(result);

    }
    @Test
    public void correctMove(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(2,4,new Piece(PieceType.KING,PieceSet.WHITE));
        board.setPiece(0,3,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        Move move = new Move("C4","D4");
        boolean result = kingMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void correctMove2(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(0,3,new Piece(PieceType.KING,PieceSet.WHITE));
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        Move move = new Move("A5","B4");
        boolean result = kingMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void correctMove3(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(7,3,new Piece(PieceType.KING,PieceSet.WHITE));
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        Move move = new Move("H5","G5");
        boolean result = kingMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void correctMove2Black(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,0,null);
        board.setPiece(0,3,new Piece(PieceType.KING,PieceSet.BLACK));
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        Move move = new Move("A5","A4");
        boolean result = kingMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void noSpaceEpmtyAroundOpponentBeat2(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(3, 4, new Piece(PieceType.KING, PieceSet.WHITE));
        board.setPiece(7,3,new Piece(PieceType.ROOK, PieceSet.BLACK));
        board.setPiece(7,4,new Piece(PieceType.ROOK, PieceSet.BLACK));
        board.setPiece(0,4,new Piece(PieceType.ROOK, PieceSet.BLACK));
        Move move = new Move("D4","D3");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void OnePossisNoEmpty2(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(2,4,new Piece(PieceType.KING,PieceSet.WHITE));
        board.setPiece(0,3,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        Move move = new Move("C4","B5");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertTrue(result);

    }
    @Test
    public void leftCorrect() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4, 0, null);
        board.setPiece(2, 4, new Piece(PieceType.KING, PieceSet.BLACK));
        board.setPiece(4, 3, new Piece(PieceType.BISHOP, PieceSet.WHITE));
        Move move = new Move("C4","B4");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertTrue(result);
    }
    @Test
    public void tryBeatOwn() {
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4, 0, null);
        board.setPiece(2, 4, new Piece(PieceType.KING, PieceSet.BLACK));
        board.setPiece(2, 3, new Piece(PieceType.KING, PieceSet.BLACK));
        Move move = new Move("C4","C5");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertFalse(result);
    }
    @Test
    public void twoPossisNoEmpty3(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,7,null);
        board.setPiece(2,4,new Piece(PieceType.KING,PieceSet.WHITE));
        board.setPiece(4,3,new Piece(PieceType.BISHOP,PieceSet.BLACK));
        Move move = new Move("C4","C5");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertTrue(result);

    }
    @Test
    public void correctMove12(){
        ChessGameBoard board = new ChessGameBoard();
        board.setPiece(4,6,null);
        Move move = new Move("E1","E2");
        KingMoveValidator kingMoveValidator = new KingMoveValidator();
        boolean result = kingMoveValidator.isValid(board,move);
        assertTrue(result);

    }






    }
